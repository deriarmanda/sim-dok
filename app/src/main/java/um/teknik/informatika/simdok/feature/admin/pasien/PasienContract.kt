package um.teknik.informatika.simdok.feature.admin.pasien

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Pasien

interface PasienContract {

    interface Presenter : BasePresenter {
        var list: List<Pasien>
        fun loadListPasien()
        fun searchPasien(query: String, filter: Int)
        fun openDetailPage(pasien: Pasien)
    }

    interface View : BaseView<Presenter> {
        fun showSearchPage()
        fun closeSearchPage()
        fun showListPasien(list: List<Pasien>)
        fun showEmptyListMessage()
        fun onLoadListPasienFailed(@StringRes msgRes: Int)
        fun showDetailPage(pasien: Pasien)
    }
}