package um.teknik.informatika.simdok.feature.profil.dokter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.Dokter;

public class ProfilDokterRvAdapter extends RecyclerView.Adapter<ProfilDokterRvAdapter.MyViewHolder> {

    private List<Dokter> mData;

    ProfilDokterRvAdapter() {
        mData = new ArrayList<>();
    }

    public void setData(List<Dokter> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dokter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textItemNama.setText(mData.get(position).getNama());
        holder.textItemAlamat.setText(mData.get(position).getAlamat());
        holder.textJadwalSenin.setText(mData.get(position).getJadwalSenin());
        holder.textJadwalSelasa.setText(mData.get(position).getJadwalSelasa());
        holder.textJadwalRabu.setText(mData.get(position).getJadwalRabu());
        holder.textJadwalKamis.setText(mData.get(position).getJadwalKamis());
        holder.textJadwalJumat.setText(mData.get(position).getJadwalJumat());
        holder.textJadwalSabtu.setText(mData.get(position).getJadwalSabtu());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textItemNama;
        TextView textItemAlamat;
        TextView textJadwalSenin, textJadwalSelasa, textJadwalRabu,
                textJadwalKamis, textJadwalJumat, textJadwalSabtu;

        MyViewHolder(View itemView) {
            super(itemView);

            textItemNama = itemView.findViewById(R.id.text_item_dokter_nama);
            textItemAlamat = itemView.findViewById(R.id.text_item_dokter_alamat);
            textJadwalSenin = itemView.findViewById(R.id.text_item_jam_senin);
            textJadwalSelasa = itemView.findViewById(R.id.text_item_jam_selasa);
            textJadwalRabu = itemView.findViewById(R.id.text_item_jam_rabu);
            textJadwalKamis = itemView.findViewById(R.id.text_item_jam_kamis);
            textJadwalJumat = itemView.findViewById(R.id.text_item_jam_jumat);
            textJadwalSabtu = itemView.findViewById(R.id.text_item_jam_sabtu);
        }
    }
}
