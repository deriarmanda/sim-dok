package um.teknik.informatika.simdok.feature.admin.antrean

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_list_antrean.view.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Antrean

class AntreanRvAdapter : RecyclerView.Adapter<AntreanRvAdapter.ViewHolder>() {

    var listAntrean = listOf<Antrean>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_antrean, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(listAntrean[position])
    }

    override fun getItemCount() = listAntrean.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(antrean: Antrean) {
            itemView.text_no_antrean.text = String.format("%d", antrean.noAntrean)
            itemView.text_nama.text = antrean.nama
            itemView.text_alamat.text = antrean.alamat
            itemView.text_dokter.text = antrean.namaDokter

            itemView.setBackgroundResource(
                    if (antrean.status == 1) R.color.light_gray
                    else R.color.solid_white
            )
        }
    }
}