package um.teknik.informatika.simdok.feature.antrian

import android.util.Log
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.model.Dokter
import um.teknik.informatika.simdok.data.repository.AntrianRepository
import um.teknik.informatika.simdok.data.repository.InfoProfilRepository

class AntreanPresenter(
        private val view: AntreanContract.View,
        private val antrianRepository: AntrianRepository,
        private val infoProfilRepository: InfoProfilRepository
) : AntreanContract.Presenter {

    override fun checkStatusAntrean() {
        view.setLoadingIndicator(true)
        if (antrianRepository.isAlreadyAntre()) loadAntrean()
        else loadListDokter()
    }

    override fun loadListDokter() {
        view.setLoadingIndicator(true)
        infoProfilRepository.getListDokter(
                object : BaseRepoCallback<List<Dokter>> {
                    override fun onSuccess(data: List<Dokter>) {
                        view.setLoadingIndicator(false)
                        view.showFormAntrianPage(data)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadListDokterFailed(R.string.error_no_internet)
                        Log.d("AntreanPresenter", msg)
                    }
                }
        )
    }

    override fun insertAntrean(antrean: Antrean) {
        view.setLoadingIndicator(true)
        antrianRepository.insertNewAntrian(
                antrean,
                object : BaseRepoCallback<Antrean> {
                    override fun onSuccess(data: Antrean) {
                        val newAntrean = data
                        antrianRepository.findNoAntrean(
                                object : BaseRepoCallback<Int> {
                                    override fun onSuccess(data: Int) {
                                        view.setLoadingIndicator(false)
                                        view.showInfoAntrean(
                                                newAntrean,
                                                data
                                        )
                                    }

                                    override fun onFailed(msg: String) {
                                        view.setLoadingIndicator(false)
                                        view.onInsertAntrianFailed(R.string.error_no_internet)
                                        Log.d("AntreanPresenter", msg)
                                    }
                                }
                        )
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onInsertAntrianFailed(R.string.error_no_internet)
                        Log.d("AntreanPresenter", msg)
                    }
                }
        )
    }

    override fun loadAntrean() {
        view.setLoadingIndicator(true)
        antrianRepository.getInfoAntrian(
                object : BaseRepoCallback<Antrean?> {
                    override fun onSuccess(data: Antrean?) {
                        if (data == null) checkStatusAntrean()
                        else {
                            val antrean = data
                            antrianRepository.findNoAntrean(
                                    object : BaseRepoCallback<Int> {
                                        override fun onSuccess(data: Int) {
                                            view.setLoadingIndicator(false)
                                            view.showInfoAntrean(
                                                    antrean,
                                                    data
                                            )
                                        }

                                        override fun onFailed(msg: String) {
                                            view.setLoadingIndicator(false)
                                            view.onLoadAntreanFailed(R.string.error_no_internet)
                                            Log.d("AntreanPresenter", msg)
                                        }
                                    }
                            )
                        }
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadAntreanFailed(R.string.error_no_internet)
                        Log.d("AntreanPresenter", msg)
                    }
                }
        )
    }

    override fun removeCurrAntrean() {
        antrianRepository.deletePassedAntrean()
    }

    override fun start() {
        checkStatusAntrean()
    }
}