package um.teknik.informatika.simdok.feature.admin.pemeriksaan.add

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_pemeriksaan.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.model.Pemeriksaan
import um.teknik.informatika.simdok.data.repository.PemeriksaanRepository
import um.teknik.informatika.simdok.util.DateTimeUtil
import java.util.*

class AddPemeriksaanActivity : AppCompatActivity(), AddPemeriksaanContract.View {

    override lateinit var presenter: AddPemeriksaanContract.Presenter
    private lateinit var snackbar: Snackbar

    companion object {
        private const val EXTRA_PASIEN = "extra_data_pasien"
        fun getIntent(context: Context, pasien: Pasien): Intent {
            val intent = Intent(context, AddPemeriksaanActivity::class.java)
            intent.putExtra(EXTRA_PASIEN, pasien)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_pemeriksaan)
        supportActionBar?.subtitle = DateTimeUtil.getHumanReadableDate(Date())

        val pasien = intent.getParcelableExtra<Pasien>(EXTRA_PASIEN)
        presenter = AddPemeriksaanPresenter(
                this,
                pasien,
                PemeriksaanRepository.getInstance()
        )

        button_simpan.setOnClickListener {
            presenter.savePemeriksaan(Pemeriksaan(
                    DateTimeUtil.getSortableCurrentDate(Date()),
                    form_anamnesa.text.toString(),
                    form_diagnosa.text.toString(),
                    form_tindakan.text.toString(),
                    form_obat.text.toString()
            ))
        }

        snackbar = Snackbar.make(
                container_btn_simpan,
                R.string.error_no_internet,
                Snackbar.LENGTH_INDEFINITE
        )
        snackbar.setAction(R.string.action_retry) {
            button_simpan.performClick()
        }
    }

    override fun onPemeriksaanSaved() {
        form_anamnesa.setText("")
        form_diagnosa.setText("")
        form_tindakan.setText("")
        form_obat.setText("")
        Toast.makeText(
                this,
                "Berhasil menambahkan data pemeriksaan.",
                Toast.LENGTH_SHORT
        ).show()
    }

    override fun onSavePemeriksaanFailed(message: String) {
        snackbar.setText(message)
        snackbar.show()
    }

    override fun setLoadingIndicator(active: Boolean) {
        snackbar.dismiss()
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

}
