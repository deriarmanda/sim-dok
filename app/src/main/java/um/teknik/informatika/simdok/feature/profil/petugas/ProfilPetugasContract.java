package um.teknik.informatika.simdok.feature.profil.petugas;

import android.support.annotation.StringRes;

import java.util.List;

import um.teknik.informatika.simdok.base.BasePresenter;
import um.teknik.informatika.simdok.base.BaseView;
import um.teknik.informatika.simdok.data.model.Petugas;

public interface ProfilPetugasContract {
    interface Presenter extends BasePresenter {
        void loadListPetugasFromServer();
    }

    interface View extends BaseView<Presenter> {
        void onLoadListPetugasSucceed(List<Petugas> petugasList);
        void showEmptyListMessage();
        void onLoadListPetugasFailed(@StringRes int msgRes);
    }
}
