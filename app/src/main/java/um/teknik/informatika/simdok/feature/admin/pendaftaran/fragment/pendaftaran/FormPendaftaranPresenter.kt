package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran

import android.util.Log
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.repository.PasienRepository

class FormPendaftaranPresenter(
        private val view: FormPendaftaranContract.View,
        private val pasienRepo: PasienRepository
) : FormPendaftaranContract.Presenter {

    override fun loadDataPasien(pasien: Pasien) {
        view.setLoadingIndicator(true)
        view.fetchDataPasien(pasien)
        view.setLoadingIndicator(false)
    }

    override fun savePasienBaru(pasien: Pasien) {
        view.setLoadingIndicator(true)
        pasienRepo.insertPasienBaru(
                pasien,
                object : BaseRepoCallback<Pasien> {
                    override fun onSuccess(data: Pasien) {
                        view.setLoadingIndicator(false)
                        view.onSavePasienSuccess(data)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onSavePasienFailed(R.string.error_no_internet)
                        Log.d("FormDaftarPresenter", msg)
                    }
                }
        )
    }

    override fun updatePasienLama(pasien: Pasien) {
        view.setLoadingIndicator(true)
        pasienRepo.updatePasienLama(
                pasien,
                object : BaseRepoCallback<Pasien> {
                    override fun onSuccess(data: Pasien) {
                        view.setLoadingIndicator(false)
                        view.onSavePasienSuccess(data)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onSavePasienFailed(R.string.error_no_internet)
                        Log.d("FormDaftarPresenter", msg)
                    }
                }
        )
    }

    override fun start() {}

}