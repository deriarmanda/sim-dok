package um.teknik.informatika.simdok.data.model

data class Klinik(
        val nama: String = "",
        val alamat: String = "",
        val deskripsi: String = "",
        val urlFoto: String = ""
)