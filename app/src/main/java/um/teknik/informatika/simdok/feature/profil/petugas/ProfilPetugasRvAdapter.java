package um.teknik.informatika.simdok.feature.profil.petugas;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.Petugas;

public class ProfilPetugasRvAdapter extends RecyclerView.Adapter<ProfilPetugasRvAdapter.MyViewHolder> {
    private List<Petugas> mData;

    ProfilPetugasRvAdapter() {
        this.mData = new ArrayList<>();
    }

    public void setData(List<Petugas> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_petugas, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textItemNama.setText(mData.get(position).getNama());
        holder.textItemAlamat.setText(mData.get(position).getAlamat());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textItemNama;
        TextView textItemAlamat;

        MyViewHolder(View itemView) {
            super(itemView);

            textItemNama = itemView.findViewById(R.id.text_item_petugas_nama);
            textItemAlamat = itemView.findViewById(R.id.text_item_petugas_alamat);
        }
    }
}
