package um.teknik.informatika.simdok.base

interface BaseRepoCallback<T> {
    fun onSuccess(data: T)
    fun onFailed(msg: String)
}