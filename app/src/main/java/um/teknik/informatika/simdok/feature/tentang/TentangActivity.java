package um.teknik.informatika.simdok.feature.tentang;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import um.teknik.informatika.sim_dok.R;

public class TentangActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang);
    }
}
