package um.teknik.informatika.simdok.feature.admin.klinik.list.diagnosis;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import kotlin.Unit;
import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.base.BaseRepoCallback;
import um.teknik.informatika.simdok.data.model.DataKlinik;
import um.teknik.informatika.simdok.data.repository.DataKlinikRepository;

import static um.teknik.informatika.simdok.data.model.DataKlinikKt.TYPE_DIAGNOSA;

public class DaftarDiagnosisPresenter implements DaftarDiagnosisContract.Presenter {

    private DaftarDiagnosisContract.View mView;
    private DataKlinikRepository mDataKlinikRepo;

    DaftarDiagnosisPresenter(DaftarDiagnosisContract.View view) {
        mView = view;
        mDataKlinikRepo = DataKlinikRepository.getInstance();
    }

    @Override
    public void loadListDiagnosisFromServer() {
        mView.setLoadingIndicator(true);
        mDataKlinikRepo.getListDataKlinik(
                TYPE_DIAGNOSA,
                new BaseRepoCallback<List<DataKlinik>>() {
                    @Override
                    public void onSuccess(List<DataKlinik> data) {
                        mView.setLoadingIndicator(false);
                        if (data.isEmpty()) mView.showEmptyListMessage();
                        else mView.onLoadListDiagnosisSucceed(data);
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onLoadListDiagnosisFailed(R.string.error_no_internet);
                    }
                }
        );
    }

    @Override
    public void deleteSelectedDiagnosis(DataKlinik diagnosis) {
        mView.setLoadingIndicator(true);
        mDataKlinikRepo.deleteDataKlinikSucceed(
                TYPE_DIAGNOSA,
                diagnosis,
                new BaseRepoCallback<Unit>() {
                    @Override
                    public void onSuccess(Unit data) {
                        mView.setLoadingIndicator(false);
                        mView.onDeleteDiagnosisSucceed();
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onDeleteDiagnosisFailed();
                    }
                }
        );
    }

    @Override
    public void start() {
        loadListDiagnosisFromServer();
    }
}
