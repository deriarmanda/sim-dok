package um.teknik.informatika.simdok.feature.home

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Antrean

interface HomeContract {

    interface Presenter : BasePresenter {
        fun checkUserAlreadySignedIn()
        fun loadInfoAntrean()
    }

    interface View : BaseView<Presenter> {
        fun openAdminHomePage()
        fun onLoadInfoAntreanSucceed(antrean: Antrean, currentAntrean: Int)
        fun onLoadInfoAntreanFailed(@StringRes msgRes: Int)
        fun showEmptyInfoAntrean()
    }
}