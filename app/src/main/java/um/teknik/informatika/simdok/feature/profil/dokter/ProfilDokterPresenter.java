package um.teknik.informatika.simdok.feature.profil.dokter;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.base.BaseRepoCallback;
import um.teknik.informatika.simdok.data.model.Dokter;
import um.teknik.informatika.simdok.data.repository.InfoProfilRepository;

public class ProfilDokterPresenter implements ProfilDokterContract.Presenter {

    private ProfilDokterContract.View mView;
    private InfoProfilRepository mProfilRepo;

    ProfilDokterPresenter(ProfilDokterContract.View view) {
        mView = view;
        mProfilRepo = InfoProfilRepository.Companion.getInstance();
    }

    @Override
    public void loadListDokterFromServer() {
        mView.setLoadingIndicator(true);
        mProfilRepo.getListDokter(
                new BaseRepoCallback<List<Dokter>>() {
                    @Override
                    public void onSuccess(List<Dokter> data) {
                        mView.setLoadingIndicator(false);
                        if (data.isEmpty()) mView.showEmptyListMessage();
                        else mView.onLoadListDokterSucceed(data);
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onLoadListDokterFailed(R.string.error_no_internet);
                        Log.d("ProfilDokterPresenter", msg);
                    }
                }
        );
    }

    @Override
    public void start() {
        loadListDokterFromServer();
    }
}
