package um.teknik.informatika.simdok.feature.admin.login

import um.teknik.informatika.simdok.data.repository.AccountRepository

class LoginPresenter(
        private val view: LoginContract.View,
        private val accountRepo: AccountRepository
) : LoginContract.Presenter {

    private val accountCallback: AccountRepository.AccountCallback

    init {
        accountCallback = object : AccountRepository.AccountCallback {
            override fun onLoginSucceed() {
                view.setLoadingIndicator(false)
                view.onLoginSucceed()
            }

            override fun onTaskFailed(msgRes: Int) {
                view.setLoadingIndicator(false)
                view.onLoginFailed(msgRes)
            }
        }
    }

    override fun doLogin(email: String, password: String) {
        view.setLoadingIndicator(true)
        accountRepo.loginToServer(email, password, accountCallback)
    }

    override fun start() {}
}