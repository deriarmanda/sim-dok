package um.teknik.informatika.simdok.feature.admin.pemeriksaan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_pemeriksaan.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.model.Pemeriksaan
import um.teknik.informatika.simdok.data.repository.PemeriksaanRepository
import um.teknik.informatika.simdok.feature.admin.pemeriksaan.add.AddPemeriksaanActivity

class PemeriksaanActivity : AppCompatActivity(), PemeriksaanContract.View {

    override lateinit var presenter: PemeriksaanContract.Presenter
    private lateinit var pasien: Pasien
    private lateinit var snackbar: Snackbar
    private val buttonDeleteListener =
            object : PemeriksaanRvAdapter.OnButtonDeleteClickListener {
                override fun onClick(pemeriksaan: Pemeriksaan) {
                    presenter.deletePemeriksaan(pemeriksaan)
                }
            }
    private val listAdapter = PemeriksaanRvAdapter(buttonDeleteListener)

    companion object {
        private const val EXTRA_PASIEN = "extra_data_pasien"
        fun getIntent(context: Context, pasien: Pasien): Intent {
            val intent = Intent(context, PemeriksaanActivity::class.java)
            intent.putExtra(EXTRA_PASIEN, pasien)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pemeriksaan)

        pasien = intent.getParcelableExtra(EXTRA_PASIEN)
        supportActionBar?.subtitle = String.format(
                "%s / %s",
                pasien.nama,
                pasien.noRm
        )
        presenter = PemeriksaanPresenter(
                this,
                pasien,
                PemeriksaanRepository.getInstance()
        )

        with(refresh_layout) {
            setColorSchemeResources(
                    R.color.accent,
                    R.color.primary
            )
            setOnRefreshListener {
                presenter.loadListPemeriksaan()
            }
        }
        with(list_pemeriksaan) {
            val layout = LinearLayoutManager(this@PemeriksaanActivity)
            layout.stackFromEnd = true
            layout.reverseLayout = true
            layoutManager = layout
            adapter = listAdapter
        }
        fab_add_pemeriksaan.setOnClickListener {
            showAddPemeriksaanPage()
        }
        snackbar = Snackbar.make(
                fab_add_pemeriksaan,
                R.string.error_no_internet,
                Snackbar.LENGTH_INDEFINITE
        )
        snackbar.setAction(R.string.action_retry) {
            presenter.loadListPemeriksaan()
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListPemeriksaan(list: List<Pemeriksaan>) {
        text_msg_empty.visibility = View.GONE
        list_pemeriksaan.visibility = View.VISIBLE
        listAdapter.list = list
    }

    override fun showEmptyListMessage() {
        text_msg_empty.visibility = View.VISIBLE
        list_pemeriksaan.visibility = View.GONE
    }

    override fun onLoadListPemeriksaanFailed(message: String) {
        snackbar.setText(message)
        snackbar.show()
    }

    override fun onDeletePasienSuccess() {
        Toast.makeText(
                this,
                "Berhasil menghapus data pemeriksaan terkait.",
                Toast.LENGTH_SHORT
        ).show()
        presenter.loadListPemeriksaan()
    }

    override fun onDeletePasienFailed(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showAddPemeriksaanPage() {
        startActivity(AddPemeriksaanActivity.getIntent(this, pasien))
    }

    override fun setLoadingIndicator(active: Boolean) {
        snackbar.dismiss()
        refresh_layout.isRefreshing = active
    }

}
