package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment

import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran.FormPendaftaranPresenter
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienPresenter

interface FragmentsInteractionListener {
    fun getFormPresenter(): FormPendaftaranPresenter
    fun getSearchPresenter(): SearchPasienPresenter
    fun onSearchResultItemClick(pasien: Pasien)
}