package um.teknik.informatika.simdok.data.model

import com.google.firebase.database.Exclude

data class Pemeriksaan(
        val tanggal: String = "",
        val anamnesa: String = "",
        val diagnosa: String = "",
        val tindakan: String = "",
        val obat: String = "",
        @get:Exclude var uid: String = ""
)