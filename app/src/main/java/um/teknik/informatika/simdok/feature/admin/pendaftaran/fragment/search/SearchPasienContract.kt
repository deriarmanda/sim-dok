package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Pasien

interface SearchPasienContract {

    interface Presenter : BasePresenter {
        var list: List<Pasien>
        fun doSearch(query: String)
        fun loadListPasien()
    }

    interface View : BaseView<Presenter> {
        companion object {
            const val SEARCH_BY_NO_RM = 301
            const val SEARCH_BY_NAMA = 302
        }

        fun getSearchFilter(): Int
        fun showSearchResult(list: List<Pasien>)
        fun onLoadPasienFailed(@StringRes msgRes: Int)
    }
}