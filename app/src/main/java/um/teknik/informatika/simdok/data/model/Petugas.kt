package um.teknik.informatika.simdok.data.model

import com.google.firebase.database.Exclude

data class Petugas(
        val nama: String = "",
        val alamat: String = "",
        @Exclude var uid: String = ""
)