package um.teknik.informatika.simdok.feature.admin.pasien

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.activity_pasien.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.repository.PasienRepository
import um.teknik.informatika.simdok.feature.admin.pasien.detail.DetailPasienActivity
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NAMA
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NO_RM

class PasienActivity : AppCompatActivity(), PasienContract.View {

    override lateinit var presenter: PasienContract.Presenter
    private lateinit var snackbar: Snackbar
    private val itemClickListener =
            object : PasienRvAdapter.OnItemClickListener {
                override fun onItemClick(pasien: Pasien) {
                    presenter.openDetailPage(pasien)
                }
            }
    private val listAdapter = PasienRvAdapter(itemClickListener)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pasien)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter = PasienPresenter(
                this,
                PasienRepository.getInstance()
        )

        with(refresh_layout) {
            setColorSchemeResources(
                    R.color.accent,
                    R.color.primary
            )
            setOnRefreshListener {
                presenter.loadListPasien()
            }
        }
        with(list_pasien) {
            val reverse = LinearLayoutManager(this@PasienActivity)
            reverse.reverseLayout = true
            reverse.stackFromEnd = true
            layoutManager = reverse
            adapter = listAdapter
        }
        with(search_view) {
            setOnSearchViewListener(
                    object : MaterialSearchView.SearchViewListener {
                        override fun onSearchViewClosed() {
                            closeSearchPage()
                            hideKeyboard(root)
                            presenter.searchPasien("", SEARCH_BY_NO_RM)
                        }

                        override fun onSearchViewShown() {
                            showSearchPage()
                        }
                    }
            )
            setOnQueryTextListener(
                    object : MaterialSearchView.OnQueryTextListener {
                        override fun onQueryTextSubmit(query: String?): Boolean {
                            val filter =
                                    if (spinner_filter.selectedItem == "No RM") SEARCH_BY_NO_RM
                                    else SEARCH_BY_NAMA
                            presenter.searchPasien(
                                    query ?: "",
                                    filter
                            )
                            return true
                        }

                        override fun onQueryTextChange(newText: String?) = false
                    }
            )
        }
        snackbar = Snackbar.make(root, "", Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.action_retry) {
            presenter.loadListPasien()
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onBackPressed() {
        if (search_view.isSearchOpen) search_view.closeSearch()
        else super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.material_search_view, menu)
        search_view.setMenuItem(menu?.findItem(R.id.action_search))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun showSearchPage() {
        card_filter.visibility = View.VISIBLE
    }

    override fun closeSearchPage() {
        card_filter.visibility = View.GONE
    }

    override fun showListPasien(list: List<Pasien>) {
        text_msg_empty.visibility = View.GONE
        list_pasien.visibility = View.VISIBLE
        listAdapter.list = list
    }

    override fun showEmptyListMessage() {
        text_msg_empty.visibility = View.VISIBLE
        list_pasien.visibility = View.GONE
    }

    override fun onLoadListPasienFailed(msgRes: Int) {
        snackbar.setText(msgRes)
        snackbar.show()
    }

    override fun showDetailPage(pasien: Pasien) {
        startActivity(DetailPasienActivity.getIntent(this, pasien))
    }

    override fun setLoadingIndicator(active: Boolean) {
        snackbar.dismiss()
        refresh_layout.isRefreshing = active
    }
}
