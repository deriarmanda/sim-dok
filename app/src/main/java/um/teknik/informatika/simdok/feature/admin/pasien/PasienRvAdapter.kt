package um.teknik.informatika.simdok.feature.admin.pasien

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_pasien.view.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.util.DateTimeUtil

class PasienRvAdapter(
        private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<PasienRvAdapter.ViewHolder>() {

    var list = listOf<Pasien>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_pasien, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(list[position])
    }

    override fun getItemCount() = list.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(pasien: Pasien) {
            itemView.text_nama.text = pasien.nama
            itemView.text_no_rm.text = pasien.noRm
            itemView.text_ttl.text = String.format(
                    "%s, %s",
                    pasien.tempatLahir,
                    pasien.tglLahir
            )
            itemView.text_berkunjung.text = String.format(
                    "Terakhir berkunjung %s",
                    DateTimeUtil.formatHumanReadableDateString(pasien.tglBerkunjung)
            )
            itemView.image_pasien.setImageResource(
                    if (pasien.jenisKelamin == "Laki-laki") R.drawable.ic_person_male_colored_64dp
                    else R.drawable.ic_person_female_colored_64dp
            )
            itemView.button_detail.setOnClickListener {
                itemClickListener.onItemClick(pasien)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(pasien: Pasien)
    }
}