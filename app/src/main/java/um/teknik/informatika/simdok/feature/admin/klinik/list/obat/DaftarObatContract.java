package um.teknik.informatika.simdok.feature.admin.klinik.list.obat;

import android.support.annotation.StringRes;

import java.util.List;

import um.teknik.informatika.simdok.base.BasePresenter;
import um.teknik.informatika.simdok.base.BaseView;
import um.teknik.informatika.simdok.data.model.DataKlinik;

public interface DaftarObatContract {

    interface Presenter extends BasePresenter {
        void loadListObatFromServer();

        void deleteSelectedObat(DataKlinik obat);
    }

    interface View extends BaseView<Presenter> {
        void onLoadListObatSucceed(List<DataKlinik> dataKlinikList);
        void onLoadListObatFailed(@StringRes int msgRes);

        void showEmptyListMessage();

        void onDeleteObatSucceed();

        void onDeleteObatFailed();
    }
}
