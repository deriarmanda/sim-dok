package um.teknik.informatika.simdok.feature.admin.pendaftaran

import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran.FormPendaftaranPresenter
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienPresenter

interface PendaftaranContract {

    interface Presenter : BasePresenter {
        fun getFormPresenter(): FormPendaftaranPresenter
        fun getSearchPresenter(): SearchPasienPresenter
        fun sendSearchQuery(query: String)
        fun sendDataPasien(pasien: Pasien)
    }

    interface View : BaseView<Presenter> {
        fun showFormPage()
        fun showSearchPage()
    }
}