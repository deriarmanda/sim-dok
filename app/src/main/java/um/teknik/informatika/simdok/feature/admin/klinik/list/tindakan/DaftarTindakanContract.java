package um.teknik.informatika.simdok.feature.admin.klinik.list.tindakan;

import android.support.annotation.StringRes;

import java.util.List;

import um.teknik.informatika.simdok.base.BasePresenter;
import um.teknik.informatika.simdok.base.BaseView;
import um.teknik.informatika.simdok.data.model.DataKlinik;

public interface DaftarTindakanContract {

    interface Presenter extends BasePresenter {
        void loadListTindakanFromServer();

        void deleteSelectedTindakan(DataKlinik tindakan);
    }

    interface View extends BaseView<Presenter> {
        void onLoadListTindakanSucceed(List<DataKlinik> dataKlinikList);
        void onLoadListTindakanFailed(@StringRes int msgRes);

        void showEmptyListMessage();

        void onDeleteTindakanSucceed();

        void onDeleteTindakanFailed();
    }
}
