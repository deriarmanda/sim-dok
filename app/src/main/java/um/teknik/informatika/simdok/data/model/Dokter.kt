package um.teknik.informatika.simdok.data.model

import com.google.firebase.database.Exclude

data class Dokter(
        val nama: String = "",
        val alamat: String = "",
        val jadwalSenin: String = "",
        val jadwalSelasa: String = "",
        val jadwalRabu: String = "",
        val jadwalKamis: String = "",
        val jadwalJumat: String = "",
        val jadwalSabtu: String = "",
        @Exclude var uid: String = ""
)