package um.teknik.informatika.simdok.feature.admin.home

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home_admin.*
import kotlinx.android.synthetic.main.app_bar_home_admin.*
import kotlinx.android.synthetic.main.content_home_admin.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.repository.AccountRepository
import um.teknik.informatika.simdok.feature.admin.antrean.ManageAntreanActivity
import um.teknik.informatika.simdok.feature.admin.klinik.KlinikActivity
import um.teknik.informatika.simdok.feature.admin.laporan.LaporanActivity
import um.teknik.informatika.simdok.feature.admin.pasien.PasienActivity
import um.teknik.informatika.simdok.feature.admin.pendaftaran.PendaftaranActivity
import um.teknik.informatika.simdok.feature.bantuan.BantuanActivity
import um.teknik.informatika.simdok.feature.home.HomeActivity
import um.teknik.informatika.simdok.feature.profil.dokter.ProfilDokterActivity
import um.teknik.informatika.simdok.feature.profil.klinik.ProfilKlinikActivity
import um.teknik.informatika.simdok.feature.profil.petugas.ProfilPetugasActivity
import um.teknik.informatika.simdok.feature.tentang.TentangActivity

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var accountRepo: AccountRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_admin)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        accountRepo = AccountRepository.getInstance()

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        val header = nav_view.getHeaderView(0)
        val navHeaderEmail = header.findViewById<TextView>(R.id.text_email)
        navHeaderEmail.text = accountRepo.getUserEmail()

        menu_antrian.setOnClickListener {
            startActivity(ManageAntreanActivity.getIntent(this))
        }
        menu_pendaftaran.setOnClickListener {
            startActivity(PendaftaranActivity.getIntent(this))
        }
        menu_pasien.setOnClickListener {
            startActivity(Intent(this, PasienActivity::class.java))
        }
        menu_klinik.setOnClickListener {
            startActivity(Intent(this, KlinikActivity::class.java))
        }
        menu_laporan.setOnClickListener {
            startActivity(Intent(this, LaporanActivity::class.java))
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_queue_add -> {
                startActivity(PendaftaranActivity.getIntent(this))
            }
            R.id.nav_antrean_pasien -> {
                startActivity(ManageAntreanActivity.getIntent(this))
            }
            R.id.nav_data_pasien -> {
                startActivity(Intent(this, PasienActivity::class.java))
            }
            R.id.nav_data_klinik -> {
                startActivity(Intent(this, KlinikActivity::class.java))
            }
            R.id.nav_laporan -> {
                startActivity(Intent(this, LaporanActivity::class.java))
            }
            R.id.nav_clinic -> {
                startActivity(Intent(this, ProfilKlinikActivity::class.java))
            }
            R.id.nav_doctor -> {
                startActivity(Intent(this, ProfilDokterActivity::class.java))
            }
            R.id.nav_petugas -> {
                startActivity(Intent(this, ProfilPetugasActivity::class.java))
            }
            R.id.nav_help -> {
                startActivity(Intent(this, BantuanActivity::class.java))
            }
            R.id.nav_about -> {
                startActivity(Intent(this, TentangActivity::class.java))
            }
            R.id.nav_logout -> {
                accountRepo.signOut(
                        object : BaseRepoCallback<String> {
                            override fun onSuccess(data: String) {
                                Toast.makeText(
                                        this@HomeActivity,
                                        R.string.admin_msg_sign_out,
                                        Toast.LENGTH_LONG
                                ).show()
                                startActivity(Intent(
                                        this@HomeActivity,
                                        HomeActivity::class.java
                                ))
                                finish()
                            }

                            override fun onFailed(msg: String) {
                                Toast.makeText(
                                        this@HomeActivity,
                                        msg,
                                        Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                )
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
