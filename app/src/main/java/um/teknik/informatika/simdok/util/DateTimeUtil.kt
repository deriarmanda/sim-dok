package um.teknik.informatika.simdok.util

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtil {

    private const val HUMAN_DAY_DATE_PATTERN = "EEEE, d MMMM yyyy"
    private const val HUMAN_DATE_PATTERN = "d MMMM yyyy"
    private const val DATE_PATTERN = "yyyy-MM-dd"
    private const val TIME_PATTERN = "kk:mm"
    private val HUMAN_DAY_FORMATTER = SimpleDateFormat(HUMAN_DAY_DATE_PATTERN, Locale.getDefault())
    private val HUMAN_FORMATTER = SimpleDateFormat(HUMAN_DATE_PATTERN, Locale.getDefault())
    private val DATE_FORMATTER = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
    private val TIME_FORMATTER = SimpleDateFormat(TIME_PATTERN, Locale.getDefault())

    @JvmStatic
    fun formatHumanReadableDateString(dateString: String): String {
        val date = parseSortableCurrentDate(dateString)
        return HUMAN_FORMATTER.format(date)
    }

    @JvmStatic
    fun getHumanReadableDate(date: Date) = HUMAN_DAY_FORMATTER.format(date)!!

    @JvmStatic
    fun getSortableCurrentDate(date: Date) = DATE_FORMATTER.format(date)!!

    @JvmStatic
    fun getSortableCurrentTime(time: Date) = TIME_FORMATTER.format(time)!!

    private fun parseSortableCurrentDate(dateString: String) = DATE_FORMATTER.parse(dateString)

}