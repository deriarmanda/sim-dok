package um.teknik.informatika.simdok.feature.profil.petugas;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.base.BaseRepoCallback;
import um.teknik.informatika.simdok.data.model.Petugas;
import um.teknik.informatika.simdok.data.repository.InfoProfilRepository;

public class ProfilPetugasPresenter implements ProfilPetugasContract.Presenter {

    private ProfilPetugasContract.View mView;
    private InfoProfilRepository mProfilRepo;

    ProfilPetugasPresenter(ProfilPetugasContract.View view) {
        mView = view;
        mProfilRepo = InfoProfilRepository.Companion.getInstance();
    }

    @Override
    public void loadListPetugasFromServer() {
        mView.setLoadingIndicator(true);
        mProfilRepo.getListPetugas(
                new BaseRepoCallback<List<Petugas>>() {
                    @Override
                    public void onSuccess(List<Petugas> data) {
                        mView.setLoadingIndicator(false);
                        if (data.isEmpty()) mView.showEmptyListMessage();
                        else mView.onLoadListPetugasSucceed(data);
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onLoadListPetugasFailed(R.string.error_no_internet);
                        Log.d("ProfilPetugasPresenter", msg);
                    }
                }
        );
    }

    @Override
    public void start() {
        loadListPetugasFromServer();
    }
}
