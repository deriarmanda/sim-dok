package um.teknik.informatika.simdok.feature.antrian

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.model.Dokter

interface AntreanContract {

    interface Presenter : BasePresenter {
        fun checkStatusAntrean()
        fun loadListDokter()
        fun insertAntrean(antrean: Antrean)
        fun loadAntrean()
        fun removeCurrAntrean()
    }

    interface View : BaseView<Presenter> {
        fun showFormAntrianPage(dokterList: List<Dokter>)
        fun onLoadListDokterFailed(@StringRes msgRes: Int)
        fun showInfoAntrianPage()
        fun onInsertAntrianSucceed()
        fun onInsertAntrianFailed(@StringRes msgRes: Int)
        fun showInfoAntrean(antrean: Antrean, currentAntrean: Int)
        fun onLoadAntreanFailed(@StringRes msgRes: Int)
    }
}