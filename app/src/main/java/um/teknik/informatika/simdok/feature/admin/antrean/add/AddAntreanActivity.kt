package um.teknik.informatika.simdok.feature.admin.antrean.add

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_antrean.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.model.Dokter
import um.teknik.informatika.simdok.data.repository.InfoProfilRepository
import um.teknik.informatika.simdok.data.repository.ManageAntreanRepository
import um.teknik.informatika.simdok.util.DateTimeUtil
import java.util.*

class AddAntreanActivity : AppCompatActivity(), AddAntreanContract.View {

    override lateinit var presenter: AddAntreanContract.Presenter
    private lateinit var spinnerAdapter: ArrayAdapter<String>

    companion object {
        fun getIntent(context: Context) = Intent(context, AddAntreanActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_antrean)

        presenter = AddAntreanPresenter(
                this,
                ManageAntreanRepository.getInstance(),
                InfoProfilRepository.getInstance()
        )

        spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item)
        spinner_antrean.adapter = spinnerAdapter

        button_antre.setOnClickListener {
            presenter.insertAntrean(Antrean(
                    DateTimeUtil.getSortableCurrentTime(Date()),
                    form_nama.text.toString(),
                    form_alamat.text.toString(),
                    form_tgl_lahir.text.toString(),
                    spinner_antrean.selectedItem.toString(),
                    0, "", -1
            ))
        }

        presenter.start()
    }

    override fun showListDokter(list: List<Dokter>) {
        spinnerAdapter.clear()
        for (dokter in list) spinnerAdapter.add(dokter.nama)
        spinnerAdapter.notifyDataSetChanged()
    }

    override fun onLoadListDokterFailed() {
        Snackbar.make(root, R.string.error_no_internet, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry) {
                    presenter.loadListDokter()
                }
    }

    override fun onInsertAntreanSuccess() {
        form_nama.setText("")
        form_alamat.setText("")
        form_tgl_lahir.setText("")
        Toast.makeText(this, "Berhasil menambahkan antrean.", Toast.LENGTH_SHORT).show()
    }

    override fun onInsertAntreanFailed() {
        Snackbar.make(root, R.string.error_no_internet, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry) {
                    button_antre.performClick()
                }
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

}
