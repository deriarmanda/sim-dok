package um.teknik.informatika.simdok.feature.admin.pemeriksaan.add

import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Pemeriksaan

interface AddPemeriksaanContract {

    interface Presenter : BasePresenter {
        fun savePemeriksaan(pemeriksaan: Pemeriksaan)
    }

    interface View : BaseView<Presenter> {
        fun onPemeriksaanSaved()
        fun onSavePemeriksaanFailed(message: String)
    }
}