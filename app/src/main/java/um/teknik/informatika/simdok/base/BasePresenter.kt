package um.teknik.informatika.simdok.base

interface BasePresenter {
    fun start()
}