package um.teknik.informatika.simdok.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.DataKlinik

class DataKlinikRepository private constructor() {

    private val mDatabase = FirebaseDatabase.getInstance()

    companion object {
        private var sInstance: DataKlinikRepository? = null
        @JvmStatic
        fun getInstance(): DataKlinikRepository {
            if (sInstance == null) sInstance = DataKlinikRepository()
            return sInstance!!
        }
    }

    fun insertNewDataKlinik(type: String, data: DataKlinik, callback: BaseRepoCallback<Unit>) {
        val dbRef = mDatabase.getReference(type)
        dbRef.push().setValue(data) { dbError, _ ->
            if (dbError != null) callback.onFailed(dbError.message)
            else callback.onSuccess(Unit)
        }
    }

    fun deleteDataKlinikSucceed(type: String, data: DataKlinik, callback: BaseRepoCallback<Unit>) {
        val dbRef = mDatabase.getReference(type)
        dbRef.child(data.uid).removeValue { dbError, _ ->
            if (dbError != null) callback.onFailed(dbError.message)
            else callback.onSuccess(Unit)
        }
    }

    fun getListDataKlinik(type: String, callback: BaseRepoCallback<List<DataKlinik>>) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val list = arrayListOf<DataKlinik>()
                    for (data in snapshot.children) {
                        val dataKlinik = data.getValue(DataKlinik::class.java)
                        if (dataKlinik == null) {
                            callback.onFailed("NPE occured while trying to parse Data Klinik with type $type")
                            return
                        } else {
                            val uid = data.key ?: ""
                            dataKlinik.uid = uid
                            list.add(dataKlinik)
                        }
                    }
                    callback.onSuccess(list)
                } else callback.onSuccess(emptyList())
            }
        }

        val dbRef = mDatabase.getReference(type)
        dbRef.addListenerForSingleValueEvent(listener)
    }

}


