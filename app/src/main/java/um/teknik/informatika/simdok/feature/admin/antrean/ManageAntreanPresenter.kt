package um.teknik.informatika.simdok.feature.admin.antrean

import android.util.Log
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.repository.ManageAntreanRepository

class ManageAntreanPresenter(
        private val view: ManageAntreanContract.View,
        private val antreanRepo: ManageAntreanRepository
) : ManageAntreanContract.Presenter {

    override var currAntrean: Antrean = Antrean()
        set(value) {
            field = value
            if (value.idAntrean.isNotEmpty()) view.showCurrentAntreanDetail(value)
        }
    private val listAntrean = arrayListOf<Antrean>()

    override fun loadListAntrean() {
        view.setLoadingIndicator(true)
        antreanRepo.getListAntrean(
                object : BaseRepoCallback<List<Antrean>> {
                    override fun onSuccess(data: List<Antrean>) {
                        if (data.isEmpty()) view.showEmptyListMessage()
                        else {
                            view.showListAntrean(data)
                            currAntrean = Antrean(
                                    nama = "-",
                                    alamat = "-",
                                    tglLahir = "-",
                                    namaDokter = "-",
                                    noAntrean = 0
                            )
                            for (antrean in data) {
                                if (antrean.status == 0) {
                                    currAntrean = antrean
                                    break
                                }
                            }
                        }
                        listAntrean.clear()
                        listAntrean.addAll(data)
                        view.setLoadingIndicator(false)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadListAntreanFailed(R.string.error_no_internet)
                        Log.d("ManageAntreanPresenter", msg)
                    }
                }
        )
    }

    override fun forwardAntrean() {
        view.setLoadingIndicator(true)
        currAntrean.status = 1
        antreanRepo.updateAntreanStatus(
                currAntrean,
                object : BaseRepoCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        view.showMessage("Berhasil memajukan antrian.")
                        loadListAntrean()
                    }

                    override fun onFailed(msg: String) {
                        currAntrean.status = 0
                        view.setLoadingIndicator(false)
                        view.showMessage("Gagal memajukan antrian, periksa koneksi internet anda.")
                    }
                }
        )
    }

    override fun backwardAntrean() {
        if (listAntrean.isEmpty()) {
            view.showMessage("List Antrean kosong !")
            return
        }
        view.setLoadingIndicator(true)
        var prevAntrean = listAntrean[0]
        for (antrean in listAntrean) {
            if (antrean.status == 0) break
            prevAntrean = antrean
        }

        var flag = false
        if (prevAntrean.status == 0) flag = true
        prevAntrean.status = 0
        antreanRepo.updateAntreanStatus(
                prevAntrean,
                object : BaseRepoCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        view.showMessage("Berhasil memundurkan antrian.")
                        loadListAntrean()
                    }

                    override fun onFailed(msg: String) {
                        if (flag) prevAntrean.status = 0
                        else prevAntrean.status = 1

                        view.setLoadingIndicator(false)
                        view.showMessage("Gagal memundurkan antrian, periksa koneksi internet anda.")
                    }
                }
        )
    }

    override fun openPendaftaranPage() {
        if (currAntrean.idAntrean.isNotEmpty()) view.showPendaftaranPage(currAntrean)
        else view.showMessage("Data Antrean saat ini kosong !")
    }

    override fun start() = loadListAntrean()
}