package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search


import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_search_pasien.*

import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.FragmentsInteractionListener
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NAMA
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NO_RM

/**
 * A simple [Fragment] subclass.
 *
 */
class SearchPasienFragment : Fragment(), SearchPasienContract.View {

    override lateinit var presenter: SearchPasienPresenter
    private lateinit var parentListener: FragmentsInteractionListener
    private val itemResultClickListener =
            object : SearchResultRvAdapter.ItemClickListener {
                override fun onItemClick(pasien: Pasien) {
                    parentListener.onSearchResultItemClick(pasien)
                }
            }
    private val resultAdapter = SearchResultRvAdapter(itemResultClickListener)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentsInteractionListener) {
            parentListener = context
        } else throw IllegalArgumentException("Parent must be implement FragmentsInteractionListener first !")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_pasien, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(list_result) {
            val reverseLayout = LinearLayoutManager(context)
            reverseLayout.reverseLayout = true
            reverseLayout.stackFromEnd = true
            layoutManager = reverseLayout
            adapter = resultAdapter
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = parentListener.getSearchPresenter()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun getSearchFilter(): Int {
        return if (spinner_filter.selectedItem == "No RM") SEARCH_BY_NO_RM
        else SEARCH_BY_NAMA
    }

    override fun showSearchResult(list: List<Pasien>) {
        if (list.isEmpty()) {
            text_result_empty.visibility = View.VISIBLE
            list_result.visibility = View.GONE
        } else {
            text_result_empty.visibility = View.GONE
            list_result.visibility = View.VISIBLE
        }
        resultAdapter.searchMode =
                if (spinner_filter.selectedItem == "No RM") SEARCH_BY_NO_RM
                else SEARCH_BY_NAMA
        resultAdapter.listResult = list
    }

    override fun onLoadPasienFailed(msgRes: Int) {
        Snackbar.make(list_result, msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry) {
                    presenter.loadListPasien()
                }
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }
}
