package um.teknik.informatika.simdok.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude

data class Antrean(
        val waktu: String = "",
        val nama: String = "",
        val alamat: String = "",
        val tglLahir: String = "",
        val namaDokter: String = "",
        var status: Int = 0,
        @get:Exclude var idAntrean: String = "",
        @get:Exclude var noAntrean: Int = -1
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(waktu)
        parcel.writeString(nama)
        parcel.writeString(alamat)
        parcel.writeString(tglLahir)
        parcel.writeString(namaDokter)
        parcel.writeInt(status)
        parcel.writeString(idAntrean)
        parcel.writeInt(noAntrean)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Antrean> {
        override fun createFromParcel(parcel: Parcel): Antrean {
            return Antrean(parcel)
        }

        override fun newArray(size: Int): Array<Antrean?> {
            return arrayOfNulls(size)
        }
    }
}