package um.teknik.informatika.simdok.feature.admin.klinik.list.obat;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import kotlin.Unit;
import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.base.BaseRepoCallback;
import um.teknik.informatika.simdok.data.model.DataKlinik;
import um.teknik.informatika.simdok.data.repository.DataKlinikRepository;

import static um.teknik.informatika.simdok.data.model.DataKlinikKt.TYPE_OBAT;

public class DaftarObatPresenter implements DaftarObatContract.Presenter {

    private DaftarObatContract.View mView;
    private DataKlinikRepository mDataKlinikRepo;

    DaftarObatPresenter(DaftarObatContract.View view) {
        mView = view;
        mDataKlinikRepo = DataKlinikRepository.getInstance();
    }

    @Override
    public void loadListObatFromServer() {
        mView.setLoadingIndicator(true);
        mDataKlinikRepo.getListDataKlinik(
                TYPE_OBAT,
                new BaseRepoCallback<List<DataKlinik>>() {
                    @Override
                    public void onSuccess(List<DataKlinik> data) {
                        mView.setLoadingIndicator(false);
                        if (data.isEmpty()) mView.showEmptyListMessage();
                        else mView.onLoadListObatSucceed(data);
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onLoadListObatFailed(R.string.error_no_internet);
                    }
                }
        );
    }

    @Override
    public void deleteSelectedObat(DataKlinik obat) {
        mView.setLoadingIndicator(true);
        mDataKlinikRepo.deleteDataKlinikSucceed(
                TYPE_OBAT,
                obat,
                new BaseRepoCallback<Unit>() {
                    @Override
                    public void onSuccess(Unit data) {
                        mView.setLoadingIndicator(false);
                        mView.onDeleteObatSucceed();
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onDeleteObatFailed();
                    }
                }
        );
    }

    @Override
    public void start() {
        loadListObatFromServer();
    }
}
