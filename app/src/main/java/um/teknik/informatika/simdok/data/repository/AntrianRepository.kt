package um.teknik.informatika.simdok.data.repository

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.util.DateTimeUtil
import um.teknik.informatika.simdok.util.SharedPrefManager
import java.util.*

class AntrianRepository private constructor() {

    private val mDbReference =
            FirebaseDatabase.getInstance().getReference("/antrean")
    private var uidAntrean = SharedPrefManager.getAntreanUid()

    companion object {
        private var sInstance: AntrianRepository? = null
        @JvmStatic
        fun getInstance(): AntrianRepository {
            if (sInstance == null) sInstance = AntrianRepository()
            return sInstance!!
        }
    }

    fun insertNewAntrian(antrean: Antrean, callback: BaseRepoCallback<Antrean>) {
        mDbReference.child(DateTimeUtil.getSortableCurrentDate(Date()))
                .push()
                .setValue(antrean) { dbError, dbRef ->
                    if (dbError != null) callback.onFailed(dbError.message)
                    else {
                        uidAntrean = dbRef.key ?: ""
                        antrean.idAntrean = uidAntrean
                        SharedPrefManager.saveAntreanUid(uidAntrean)
                        findNoAntrean(
                                object : BaseRepoCallback<Int> {
                                    override fun onSuccess(data: Int) {
                                        antrean.noAntrean = data
                                        callback.onSuccess(antrean)
                                    }

                                    override fun onFailed(msg: String) {
                                        callback.onFailed(msg)
                                    }
                                },
                                uidAntrean
                        )
                    }
                }
    }

    fun getInfoAntrian(callback: BaseRepoCallback<Antrean?>) {
        if (uidAntrean.isEmpty()) {
            callback.onSuccess(null)
            return
        }

        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val antrean = snapshot.getValue(Antrean::class.java)
                    if (antrean != null) {
                        antrean.idAntrean = uidAntrean
                        findNoAntrean(
                                object : BaseRepoCallback<Int> {
                                    override fun onSuccess(data: Int) {
                                        antrean.noAntrean = data
                                        callback.onSuccess(antrean)
                                    }

                                    override fun onFailed(msg: String) {
                                        callback.onFailed(msg)
                                    }
                                },
                                uidAntrean
                        )
                    } else {
                        callback.onFailed("NPE occured while trying to parse " +
                                "Data Antrean with uid: $uidAntrean")
                    }
                } else {
                    deletePassedAntrean()
                    callback.onSuccess(null)
                }
            }
        }
        val currentRef = mDbReference
                .child(DateTimeUtil.getSortableCurrentDate(Date()))
                .child(uidAntrean)
        currentRef.addListenerForSingleValueEvent(listener)
    }

    fun isAlreadyAntre() = uidAntrean.isNotEmpty()

    fun findNoAntrean(callback: BaseRepoCallback<Int>, uid: String = "") {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var noAntrean = 1
                for (data in snapshot.children) {
                    val currUid = data.key
                    if (uid.isEmpty()) {
                        // find current actively no antrean
                        if (data.child("status").getValue(Int::class.java) == 0) {
                            callback.onSuccess(noAntrean)
                            return
                        } else noAntrean++
                    } else {
                        // find no antrean with specified id
                        if (currUid == uid) {
                            callback.onSuccess(noAntrean)
                            return
                        } else noAntrean++
                    }
                }
                callback.onFailed("Tidak ada data Antrean dengan uid: $uid")
            }
        }
        val todayRef = mDbReference
                .child(DateTimeUtil.getSortableCurrentDate(Date()))
                .orderByChild("waktu")
        todayRef.addListenerForSingleValueEvent(listener)
    }

    fun deletePassedAntrean() {
        SharedPrefManager.removeAntreanUid()
        uidAntrean = SharedPrefManager.getAntreanUid()
        Log.d("AntrianRepository", "current uid antrean: $uidAntrean")
    }
}