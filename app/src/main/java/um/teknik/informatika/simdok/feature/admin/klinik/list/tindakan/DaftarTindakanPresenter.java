package um.teknik.informatika.simdok.feature.admin.klinik.list.tindakan;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import kotlin.Unit;
import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.base.BaseRepoCallback;
import um.teknik.informatika.simdok.data.model.DataKlinik;
import um.teknik.informatika.simdok.data.repository.DataKlinikRepository;

import static um.teknik.informatika.simdok.data.model.DataKlinikKt.TYPE_TINDAKAN;

public class DaftarTindakanPresenter implements DaftarTindakanContract.Presenter {

    private DaftarTindakanContract.View mView;
    private DataKlinikRepository mDataKlinikRepo;

    DaftarTindakanPresenter(DaftarTindakanContract.View view) {
        mView = view;
        mDataKlinikRepo = DataKlinikRepository.getInstance();
    }

    @Override
    public void loadListTindakanFromServer() {
        mView.setLoadingIndicator(true);
        mDataKlinikRepo.getListDataKlinik(
                TYPE_TINDAKAN,
                new BaseRepoCallback<List<DataKlinik>>() {
                    @Override
                    public void onSuccess(List<DataKlinik> data) {
                        mView.setLoadingIndicator(false);
                        if (data.isEmpty()) mView.showEmptyListMessage();
                        else mView.onLoadListTindakanSucceed(data);
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onLoadListTindakanFailed(R.string.error_no_internet);
                    }
                }
        );
    }

    @Override
    public void deleteSelectedTindakan(DataKlinik tindakan) {
        mView.setLoadingIndicator(true);
        mDataKlinikRepo.deleteDataKlinikSucceed(
                TYPE_TINDAKAN,
                tindakan,
                new BaseRepoCallback<Unit>() {
                    @Override
                    public void onSuccess(Unit data) {
                        mView.setLoadingIndicator(false);
                        mView.onDeleteTindakanSucceed();
                    }

                    @Override
                    public void onFailed(@NotNull String msg) {
                        mView.setLoadingIndicator(false);
                        mView.onDeleteTindakanFailed();
                    }
                }
        );
    }

    @Override
    public void start() {
        loadListTindakanFromServer();
    }
}
