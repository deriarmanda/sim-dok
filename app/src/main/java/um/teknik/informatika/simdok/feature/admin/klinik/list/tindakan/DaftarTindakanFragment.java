package um.teknik.informatika.simdok.feature.admin.klinik.list.tindakan;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.DataKlinik;
import um.teknik.informatika.simdok.feature.admin.klinik.list.KlinikRvAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DaftarTindakanFragment extends Fragment implements DaftarTindakanContract.View {

    private DaftarTindakanContract.Presenter mPresenter;
    private RecyclerView rvTindakan;
    private SwipeRefreshLayout root;
    private LinearLayout layoutKontenKosong;
    private KlinikRvAdapter adapter;
    private Snackbar snackbar;

    public DaftarTindakanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_daftar_tindakan, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    public void onLoadListTindakanSucceed(List<DataKlinik> dataKlinikList) {
        layoutKontenKosong.setVisibility(View.GONE);
        rvTindakan.setVisibility(View.VISIBLE);
        adapter.setData(dataKlinikList);
    }

    @Override
    public void onDeleteTindakanSucceed() {
        mPresenter.loadListTindakanFromServer();
        Toast.makeText(
                getContext(),
                "Berhasil menghapus data tindakan terkait",
                Toast.LENGTH_SHORT
        ).show();
    }

    @Override
    public void onDeleteTindakanFailed() {
        Toast.makeText(
                getContext(),
                "Gagal menghapus data tindakan terkait. Periksa internet anda.",
                Toast.LENGTH_SHORT
        ).show();
    }

    @Override
    public void showEmptyListMessage() {
        layoutKontenKosong.setVisibility(View.VISIBLE);
        rvTindakan.setVisibility(View.GONE);
    }

    @Override
    public void onLoadListTindakanFailed(int msgRes) {
        snackbar.setText(msgRes);
        snackbar.show();
    }

    @Override
    public DaftarTindakanContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        snackbar.dismiss();
        root.setRefreshing(active);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter = new DaftarTindakanPresenter(this);
        rvTindakan = view.findViewById(R.id.rv_list_klinik);
        adapter = new KlinikRvAdapter(new KlinikRvAdapter.OnButtonDeleteClickListener() {
            @Override
            public void onClick(DataKlinik dataKlinik) {
                mPresenter.deleteSelectedTindakan(dataKlinik);
            }
        });
        rvTindakan.setLayoutManager(new LinearLayoutManager(getContext()));
        rvTindakan.setAdapter(adapter);
        root = view.findViewById(R.id.swipe_refresh);
        layoutKontenKosong = view.findViewById(R.id.msg_empty);
        root.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadListTindakanFromServer();
            }
        });

        snackbar = Snackbar.make(root, R.string.error_no_internet, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.loadListTindakanFromServer();
            }
        });
    }
}
