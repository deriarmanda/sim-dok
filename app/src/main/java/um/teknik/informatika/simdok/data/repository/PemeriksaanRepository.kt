package um.teknik.informatika.simdok.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pemeriksaan

class PemeriksaanRepository private constructor() {

    private val mDbReference =
            FirebaseDatabase.getInstance().getReference("/pemeriksaan")

    companion object {
        private var sInstance: PemeriksaanRepository? = null
        fun getInstance(): PemeriksaanRepository {
            if (sInstance == null) sInstance = PemeriksaanRepository()
            return sInstance!!
        }
    }

    fun insertPemeriksaan(
            uidPasien: String,
            pemeriksaan: Pemeriksaan,
            callback: BaseRepoCallback<Unit>
    ) {
        mDbReference.child(uidPasien).push()
                .setValue(pemeriksaan) { dbError, _ ->
                    if (dbError != null) callback.onFailed(dbError.message)
                    else callback.onSuccess(Unit)
                }
    }

    fun deletePemeriksaan(
            uidPasien: String,
            pemeriksaan: Pemeriksaan,
            callback: BaseRepoCallback<Unit>
    ) {
        mDbReference.child(uidPasien).child(pemeriksaan.uid)
                .removeValue { dbError, _ ->
                    if (dbError != null) callback.onFailed(dbError.message)
                    else callback.onSuccess(Unit)
                }
    }

    fun getListPemeriksaan(uidPasien: String, callback: BaseRepoCallback<List<Pemeriksaan>>) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val list = arrayListOf<Pemeriksaan>()
                    for (data in snapshot.children) {
                        val pemeriksaan = data.getValue(Pemeriksaan::class.java)
                        if (pemeriksaan == null) {
                            callback.onFailed("NPE occured while trying to parse Data Pemeriksaan.")
                            return
                        } else {
                            val uid = data.key ?: ""
                            pemeriksaan.uid = uid
                            list.add(pemeriksaan)
                        }
                    }
                    callback.onSuccess(list)
                } else callback.onSuccess(emptyList())
            }
        }

        val sortedRef = mDbReference.child(uidPasien).orderByChild("tanggal")
        sortedRef.addListenerForSingleValueEvent(listener)
    }
}