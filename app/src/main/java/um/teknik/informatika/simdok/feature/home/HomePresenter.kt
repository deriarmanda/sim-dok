package um.teknik.informatika.simdok.feature.home

import android.util.Log
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.repository.AccountRepository
import um.teknik.informatika.simdok.data.repository.AntrianRepository

class HomePresenter(
        private val view: HomeContract.View,
        private val accountRepo: AccountRepository,
        private val antreanRepo: AntrianRepository
) : HomeContract.Presenter {

    override fun checkUserAlreadySignedIn() {
        if (accountRepo.isUserSignedIn()) view.openAdminHomePage()
    }

    override fun loadInfoAntrean() {
        view.setLoadingIndicator(true)
        if (antreanRepo.isAlreadyAntre()) {
            antreanRepo.getInfoAntrian(object : BaseRepoCallback<Antrean?> {
                override fun onSuccess(data: Antrean?) {
                    if (data == null) {
                        view.setLoadingIndicator(false)
                        view.showEmptyInfoAntrean()
                    } else {
                        val antrean = data
                        antreanRepo.findNoAntrean(
                                object : BaseRepoCallback<Int> {
                                    override fun onSuccess(data: Int) {
                                        view.setLoadingIndicator(false)
                                        view.onLoadInfoAntreanSucceed(
                                                antrean,
                                                data
                                        )
                                    }

                                    override fun onFailed(msg: String) {
                                        view.setLoadingIndicator(false)
                                        view.onLoadInfoAntreanFailed(R.string.error_no_internet)
                                        Log.d("HomePresenter", msg)
                                    }
                                }
                        )
                    }
                }

                override fun onFailed(msg: String) {
                    view.setLoadingIndicator(false)
                    view.onLoadInfoAntreanFailed(R.string.error_no_internet)
                    Log.d("HomePresenter", msg)
                }
            })
        } else {
            view.showEmptyInfoAntrean()
            view.setLoadingIndicator(false)
        }
    }

    override fun start() {
        checkUserAlreadySignedIn()
        loadInfoAntrean()
    }
}