package um.teknik.informatika.simdok.feature.admin.antrean.add

import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.model.Dokter

interface AddAntreanContract {

    interface Presenter : BasePresenter {
        fun loadListDokter()
        fun insertAntrean(antrean: Antrean)
    }

    interface View : BaseView<Presenter> {
        fun showListDokter(list: List<Dokter>)
        fun onLoadListDokterFailed()
        fun onInsertAntreanSuccess()
        fun onInsertAntreanFailed()
    }
}