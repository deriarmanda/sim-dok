package um.teknik.informatika.simdok.feature.profil.dokter;

import android.support.annotation.StringRes;

import java.util.List;

import um.teknik.informatika.simdok.base.BasePresenter;
import um.teknik.informatika.simdok.base.BaseView;
import um.teknik.informatika.simdok.data.model.Dokter;

public interface ProfilDokterContract {

    interface Presenter extends BasePresenter {
        void loadListDokterFromServer();
    }

    interface View extends BaseView<Presenter> {
        void onLoadListDokterSucceed(List<Dokter> dokterList);
        void showEmptyListMessage();
        void onLoadListDokterFailed(@StringRes int msgRes);
    }
}
