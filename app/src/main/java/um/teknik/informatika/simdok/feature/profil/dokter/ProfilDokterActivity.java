package um.teknik.informatika.simdok.feature.profil.dokter;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.Dokter;

public class ProfilDokterActivity extends AppCompatActivity implements ProfilDokterContract.View {

    private ProfilDokterContract.Presenter mPresenter;
    private RecyclerView rvDokter;
    private SwipeRefreshLayout root;
    private LinearLayout layoutKontenKosong;
    private ProfilDokterRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_dokter);

        mPresenter = new ProfilDokterPresenter(this);
        bindView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    public void onLoadListDokterSucceed(List<Dokter> dokterList) {
        layoutKontenKosong.setVisibility(View.GONE);
        rvDokter.setVisibility(View.VISIBLE);
        adapter.setData(dokterList);
    }

    @Override
    public void showEmptyListMessage() {
        layoutKontenKosong.setVisibility(View.VISIBLE);
        rvDokter.setVisibility(View.GONE);
    }

    @Override
    public void onLoadListDokterFailed(int msgRes) {
        Toast.makeText(this, msgRes, Toast.LENGTH_LONG).show();
    }

    @Override
    public ProfilDokterContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        root.setRefreshing(active);
    }

    private void bindView() {
        rvDokter = findViewById(R.id.rv_profil_dokter);
        adapter = new ProfilDokterRvAdapter();
        rvDokter.setAdapter(adapter);
        rvDokter.setLayoutManager(new LinearLayoutManager(this));
        root = findViewById(R.id.swipe_refresh);
        layoutKontenKosong = findViewById(R.id.msg_empty);
        root.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadListDokterFromServer();
            }
        });
    }
}