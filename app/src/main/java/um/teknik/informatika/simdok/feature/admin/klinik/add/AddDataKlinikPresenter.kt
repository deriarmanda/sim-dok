package um.teknik.informatika.simdok.feature.admin.klinik.add

import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.DataKlinik
import um.teknik.informatika.simdok.data.repository.DataKlinikRepository

class AddDataKlinikPresenter(
        private val view: AddDataKlinikContract.View,
        private val type: String,
        private val dataKlinikRepo: DataKlinikRepository
) : AddDataKlinikContract.Presenter {

    override fun saveDataKlinik(data: DataKlinik) {
        view.setLoadingIndicator(true)
        dataKlinikRepo.insertNewDataKlinik(
                type,
                data,
                object : BaseRepoCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        view.setLoadingIndicator(false)
                        view.onDataKlinikSaved()
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onSaveDataKlinikFailed(msg)
                    }
                }
        )
    }

    override fun start() {}
}