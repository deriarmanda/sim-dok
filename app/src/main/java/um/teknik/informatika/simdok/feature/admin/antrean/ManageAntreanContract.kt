package um.teknik.informatika.simdok.feature.admin.antrean

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Antrean

interface ManageAntreanContract {

    interface Presenter : BasePresenter {
        var currAntrean: Antrean
        fun loadListAntrean()
        fun forwardAntrean()
        fun backwardAntrean()
        fun openPendaftaranPage()
    }

    interface View : BaseView<Presenter> {
        fun showListAntrean(list: List<Antrean>)
        fun showEmptyListMessage()
        fun onLoadListAntreanFailed(@StringRes msgRes: Int)
        fun showCurrentAntreanDetail(antrean: Antrean)
        fun showMessage(message: String)
        fun showPendaftaranPage(antrean: Antrean)
    }
}