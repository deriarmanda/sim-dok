package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search

import android.os.AsyncTask
import android.util.Log
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.repository.PasienRepository
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NAMA
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NO_RM

class SearchPasienPresenter(
        private val view: SearchPasienContract.View,
        private val pasienRepo: PasienRepository
) : SearchPasienContract.Presenter {

    override var list: List<Pasien> = emptyList()
    private var searchingTask = SearchingTask("", SEARCH_BY_NO_RM, view)

    override fun doSearch(query: String) {
        if (query.isBlank()) view.showSearchResult(list)
        else {
            if (!searchingTask.isCancelled) searchingTask.cancel(true)
            searchingTask = SearchingTask(query, view.getSearchFilter(), view)
            searchingTask.execute(list)
        }
    }

    override fun loadListPasien() {
        view.setLoadingIndicator(true)
        pasienRepo.getListPasien(
                object : BaseRepoCallback<List<Pasien>> {
                    override fun onSuccess(data: List<Pasien>) {
                        view.setLoadingIndicator(false)
                        list = data
                        view.showSearchResult(list)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadPasienFailed(R.string.error_no_internet)
                        Log.d("SearchPasienPresenter", msg)
                    }
                }
        )
    }

    override fun start() = loadListPasien()

    private class SearchingTask(
            private val query: String,
            private val filter: Int,
            private val view: SearchPasienContract.View
    ) : AsyncTask<List<Pasien>, Void, List<Pasien>>() {

        override fun onPreExecute() {
            super.onPreExecute()
            view.setLoadingIndicator(true)
        }

        override fun doInBackground(vararg p0: List<Pasien>?): List<Pasien> {
            val list = p0[0]!!
            val result = arrayListOf<Pasien>()
            for (pasien in list) {
                if (filter == SEARCH_BY_NO_RM && pasien.noRm.contains(query, true)) {
                    result.add(pasien)
                } else if (filter == SEARCH_BY_NAMA && pasien.nama.contains(query, true)) {
                    result.add(pasien)
                }
            }
            return result
        }

        override fun onPostExecute(result: List<Pasien>?) {
            super.onPostExecute(result)
            view.setLoadingIndicator(false)
            view.showSearchResult(result!!)
        }

    }
}