package um.teknik.informatika.simdok.data.repository

import android.support.annotation.StringRes
import com.google.firebase.auth.FirebaseAuth
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback

class AccountRepository {

    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    companion object {
        @JvmStatic
        fun getInstance() = AccountRepository()
    }

    fun isUserSignedIn(): Boolean {
        val currUser = mAuth.currentUser
        return if (currUser == null) false
        else {
            !currUser.isAnonymous
        }
    }

    fun getUserEmail() = mAuth.currentUser?.email ?: "-"

    fun loginToServer(email: String, password: String, callback: AccountCallback) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) callback.onLoginSucceed()
            else callback.onTaskFailed(R.string.login_msg_failure)
        }
    }

    fun signOut(callback: BaseRepoCallback<String>) {
        mAuth.signOut()
        mAuth.signInAnonymously().addOnCompleteListener {
            if (it.isSuccessful) callback.onSuccess("")
            else callback.onFailed(it.exception?.message!!)
        }
    }

    interface AccountCallback {
        fun onLoginSucceed()
        fun onTaskFailed(@StringRes msgRes: Int)
    }
}