package um.teknik.informatika.simdok.data.model

import com.google.firebase.database.Exclude

const val TYPE_DIAGNOSA = "/diagnosa"
const val TYPE_TINDAKAN = "/tindakan"
const val TYPE_OBAT = "/obat"

data class DataKlinik(
        val kode: String = "",
        val nama: String = "",
        @get:Exclude var uid: String = ""
)