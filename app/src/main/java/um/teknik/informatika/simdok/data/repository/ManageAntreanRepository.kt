package um.teknik.informatika.simdok.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.util.DateTimeUtil
import java.util.*

class ManageAntreanRepository private constructor() {

    private val mDbReference =
            FirebaseDatabase.getInstance().getReference("/antrean")
    private val listAntrean = arrayListOf<Antrean>()

    companion object {
        private var sInstance: ManageAntreanRepository? = null
        fun getInstance(): ManageAntreanRepository {
            if (sInstance == null) sInstance = ManageAntreanRepository()
            return sInstance!!
        }
    }

    fun getListAntrean(callback: BaseRepoCallback<List<Antrean>>) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    listAntrean.clear()
                    var noAntrean = 1
                    for (data in snapshot.children) {
                        val antrean = data.getValue(Antrean::class.java)
                        val uid = data.key
                        if (antrean != null && uid != null) {
                            antrean.idAntrean = uid
                            antrean.noAntrean = noAntrean
                            noAntrean++
                            listAntrean.add(antrean)
                        }
                        else {
                            callback.onFailed("NPE occured while trying to parse Data Antrean.")
                            return
                        }
                    }
                    callback.onSuccess(listAntrean)
                } else callback.onSuccess(listAntrean)
            }
        }

        val sortedRef = mDbReference
                .child(DateTimeUtil.getSortableCurrentDate(Date()))
                .orderByChild("waktu")
        sortedRef.addListenerForSingleValueEvent(listener)
    }

    fun updateAntreanStatus(antrean: Antrean, callback: BaseRepoCallback<Unit>) {
        mDbReference.child(DateTimeUtil.getSortableCurrentDate(Date()))
                .child(antrean.idAntrean).updateChildren(
                        mapOf(
                                Pair("/status", antrean.status))
                ) { dbError, _ ->
                    if (dbError != null) callback.onFailed(dbError.message)
                    else callback.onSuccess(Unit)
                }
    }

    fun insertAntrean(antrean: Antrean, callback: BaseRepoCallback<Unit>) {
        mDbReference.child(DateTimeUtil.getSortableCurrentDate(Date()))
                .push()
                .setValue(antrean) { dbError, _ ->
                    if (dbError != null) callback.onFailed(dbError.message)
                    else callback.onSuccess(Unit)
                }
    }
}