package um.teknik.informatika.simdok.feature.admin.antrean

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_manage_antrean.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.repository.ManageAntreanRepository
import um.teknik.informatika.simdok.feature.admin.antrean.add.AddAntreanActivity
import um.teknik.informatika.simdok.feature.admin.pendaftaran.PendaftaranActivity

class ManageAntreanActivity : AppCompatActivity(), ManageAntreanContract.View {

    override lateinit var presenter: ManageAntreanContract.Presenter
    private lateinit var snackbar: Snackbar
    private val listAdapter = AntreanRvAdapter()

    companion object {
        fun getIntent(context: Context) = Intent(context, ManageAntreanActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_antrean)

        presenter = ManageAntreanPresenter(
                this,
                ManageAntreanRepository.getInstance()
        )

        with(refresh_layout) {
            setColorSchemeResources(
                    R.color.accent,
                    R.color.primary
            )
            setOnRefreshListener {
                presenter.loadListAntrean()
            }
        }
        with(list_antrean) {
            layoutManager = LinearLayoutManager(this@ManageAntreanActivity)
            adapter = listAdapter
        }
        /*container_curr_antrean.setOnClickListener {
            presenter.openPendaftaranPage()
        }*/
        button_antre_down.setOnClickListener {
            presenter.forwardAntrean()
        }
        button_antre_up.setOnClickListener {
            presenter.backwardAntrean()
        }
        fab_add_antrean.setOnClickListener {
            startActivity(AddAntreanActivity.getIntent(this))
        }
        snackbar = Snackbar.make(refresh_layout, "", Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.action_retry) {
            presenter.loadListAntrean()
        }

        text_no_antrean.text = String.format("%d", 0)
        text_nama.text = "-"
        text_alamat.text = "-"
        text_dokter.text = "-"
        text_id_antrean.text = "-"
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListAntrean(list: List<Antrean>) {
        text_msg_empty.visibility = View.GONE
        list_antrean.visibility = View.VISIBLE
        button_antre_up.isEnabled = true
        button_antre_down.isEnabled = true
        listAdapter.listAntrean = list
        text_total_antrean.text = String.format("Total Antrean : %d", list.size)
    }

    override fun showEmptyListMessage() {
        text_msg_empty.visibility = View.VISIBLE
        list_antrean.visibility = View.GONE
        button_antre_up.isEnabled = false
        button_antre_down.isEnabled = false
        text_total_antrean.text = String.format("Total Antrean : 0")

        text_no_antrean.text = String.format("%d", 0)
        text_nama.text = "-"
        text_alamat.text = "-"
        text_dokter.text = "-"
        text_id_antrean.text = "-"
    }

    override fun onLoadListAntreanFailed(msgRes: Int) {
        snackbar.setText(msgRes)
        snackbar.show()
    }

    override fun showCurrentAntreanDetail(antrean: Antrean) {
        text_no_antrean.text = String.format("%d", antrean.noAntrean)
        text_nama.text = antrean.nama
        text_alamat.text = antrean.alamat
        text_dokter.text = antrean.namaDokter
        text_id_antrean.text = antrean.idAntrean
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showPendaftaranPage(antrean: Antrean) {
        startActivity(PendaftaranActivity.getIntent(this, antrean))
    }

    override fun setLoadingIndicator(active: Boolean) {
        snackbar.dismiss()
        refresh_layout.isRefreshing = active
    }

}
