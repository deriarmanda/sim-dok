package um.teknik.informatika.simdok.feature.splashscreen

import com.google.firebase.auth.FirebaseAuth
import um.teknik.informatika.sim_dok.R

class SplashScreenPresenter(
        private val mView: SplashScreenContract.View
) : SplashScreenContract.Presenter {

    private val mAuth = FirebaseAuth.getInstance()

    override fun checkCurrentUser() {
        mView.setLoadingIndicator(true)
        val currUser = mAuth.currentUser

        //mView.setLoadingIndicator(false)
        if (currUser == null) loginAnonymously()
        else mView.onLoginSucceed()
    }

    override fun loginAnonymously() {
        mView.setLoadingIndicator(true)

        mAuth.signInAnonymously().addOnCompleteListener {
            if (it.isSuccessful) mView.onLoginSucceed()
            else mView.onLoginFailed(R.string.error_login_anonymously)
            mView.setLoadingIndicator(false)
        }
    }

    override fun start() {
        checkCurrentUser()
    }
}