package um.teknik.informatika.simdok.feature.bantuan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import um.teknik.informatika.sim_dok.R;

public class BantuanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bantuan);
    }
}
