package um.teknik.informatika.simdok.feature.admin.pasien.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_detail_pasien.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.repository.PasienRepository
import um.teknik.informatika.simdok.feature.admin.pemeriksaan.PemeriksaanActivity

class DetailPasienActivity : AppCompatActivity() {

    private lateinit var pasienRepo: PasienRepository
    private lateinit var pasien: Pasien

    companion object {
        private const val EXTRA_PASIEN = "extra_data_pasien"
        fun getIntent(context: Context, pasien: Pasien): Intent {
            val intent = Intent(context, DetailPasienActivity::class.java)
            intent.putExtra(EXTRA_PASIEN, pasien)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pasien)

        pasienRepo = PasienRepository.getInstance()

        pasien = intent.getParcelableExtra<Pasien>(EXTRA_PASIEN)
        fetch(pasien)

        button_pemeriksaan.setOnClickListener {
            startActivity(PemeriksaanActivity.getIntent(this, pasien))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_detail_pasien, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.menu_delete) {
            progress_bar.visibility = View.VISIBLE
            pasienRepo.deletePasien(
                    pasien,
                    object : BaseRepoCallback<Unit> {
                        override fun onSuccess(data: Unit) {
                            progress_bar.visibility = View.GONE
                            Toast.makeText(
                                    this@DetailPasienActivity,
                                    "Berhasil menghapus pasien.",
                                    Toast.LENGTH_SHORT
                            ).show()
                            finish()
                        }

                        override fun onFailed(msg: String) {
                            progress_bar.visibility = View.GONE
                            Toast.makeText(
                                    this@DetailPasienActivity,
                                    "Gagal menghapus pasien. Periksa internet anda.",
                                    Toast.LENGTH_LONG
                            ).show()
                        }
                    }
            )
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun fetch(pasien: Pasien) {
        supportActionBar?.subtitle = pasien.nama
        form_no_rm.setText(pasien.noRm)
        form_nama.setText(pasien.nama)
        form_tempat_lahir.setText(pasien.tempatLahir)
        form_tanggal_lahir.setText(pasien.tglLahir)
        form_alamat.setText(pasien.alamat)
        form_telepon.setText(pasien.telp)
        form_gender.setText(pasien.jenisKelamin)
        form_gol_darah.setText(pasien.golDarah)
        form_pekerjaan.setText(pasien.pekerjaan)
        form_status.setText(pasien.status)
        form_agama.setText(pasien.agama)
    }
}
