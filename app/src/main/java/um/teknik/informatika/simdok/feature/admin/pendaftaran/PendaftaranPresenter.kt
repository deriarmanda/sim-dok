package um.teknik.informatika.simdok.feature.admin.pendaftaran

import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran.FormPendaftaranPresenter
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienPresenter

class PendaftaranPresenter(
        private val view: PendaftaranContract.View,
        private val formPresenter: FormPendaftaranPresenter,
        private val searchPresenter: SearchPasienPresenter
) : PendaftaranContract.Presenter {

    override fun getFormPresenter() = formPresenter

    override fun getSearchPresenter() = searchPresenter

    override fun sendSearchQuery(query: String) {
        searchPresenter.doSearch(query)
    }

    override fun sendDataPasien(pasien: Pasien) {
        formPresenter.loadDataPasien(pasien)
    }

    override fun start() {
        view.showFormPage()
    }
}