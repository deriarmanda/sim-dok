package um.teknik.informatika.simdok.feature.admin.klinik;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.feature.admin.klinik.add.AddDataKlinikActivity;
import um.teknik.informatika.simdok.feature.admin.klinik.list.diagnosis.DaftarDiagnosisFragment;
import um.teknik.informatika.simdok.feature.admin.klinik.list.obat.DaftarObatFragment;
import um.teknik.informatika.simdok.feature.admin.klinik.list.tindakan.DaftarTindakanFragment;

import static um.teknik.informatika.simdok.data.model.DataKlinikKt.TYPE_DIAGNOSA;
import static um.teknik.informatika.simdok.data.model.DataKlinikKt.TYPE_OBAT;
import static um.teknik.informatika.simdok.data.model.DataKlinikKt.TYPE_TINDAKAN;

public class KlinikActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.nav_daftar_diagnosis:
                    transaction.replace(R.id.content, new DaftarDiagnosisFragment()).commit();
                    return true;
                case R.id.nav_daftar_tindakan:
                    transaction.replace(R.id.content, new DaftarTindakanFragment()).commit();
                    return true;
                case R.id.nav_daftar_obat:
                    transaction.replace(R.id.content, new DaftarObatFragment()).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_klinik);

        final BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.nav_daftar_diagnosis);

        FloatingActionButton fabAdd = findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemId = navigation.getSelectedItemId();
                String type;
                switch (itemId) {
                    case R.id.nav_daftar_diagnosis:
                        type = TYPE_DIAGNOSA;
                        break;
                    case R.id.nav_daftar_tindakan:
                        type = TYPE_TINDAKAN;
                        break;
                    case R.id.nav_daftar_obat:
                        type = TYPE_OBAT;
                        break;
                    default:
                        type = TYPE_DIAGNOSA;
                        break;
                }
                startActivity(AddDataKlinikActivity.getIntent(KlinikActivity.this, type));
            }
        });
    }

}
