package um.teknik.informatika.simdok.feature.admin.pendaftaran

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.activity_pendaftaran.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.repository.PasienRepository
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.FragmentsInteractionListener
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran.FormPendaftaranFragment
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran.FormPendaftaranPresenter
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienFragment
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienPresenter

class PendaftaranActivity : AppCompatActivity(),
        PendaftaranContract.View,
        FragmentsInteractionListener {

    override lateinit var presenter: PendaftaranContract.Presenter
    private lateinit var fragmentForm: FormPendaftaranFragment
    private lateinit var fragmentSearch: SearchPasienFragment

    companion object {
        private val EXTRA_ANTREAN = "extra_data_antrean"
        fun getIntent(context: Context) = Intent(context, PendaftaranActivity::class.java)
        fun getIntent(context: Context, antrean: Antrean): Intent {
            val intent = Intent(context, PendaftaranActivity::class.java)
            intent.putExtra(EXTRA_ANTREAN, antrean)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pendaftaran)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initFragments()
        initViews()

        presenter.start()
        val antrean = intent.getParcelableExtra<Antrean>(EXTRA_ANTREAN)
        if (antrean != null) presenter.sendDataPasien(Pasien(
                nama = antrean.nama,
                alamat = antrean.alamat,
                tglLahir = antrean.tglLahir
        ))
    }

    override fun onBackPressed() {
        if (search_view.isSearchOpen) search_view.closeSearch()
        else super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.material_search_view, menu)
        search_view.setMenuItem(menu?.findItem(R.id.action_search))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun showFormPage() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.hide(fragmentSearch)
        fragmentTransaction.show(fragmentForm)
        fragmentTransaction.commit()
    }

    override fun showSearchPage() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.hide(fragmentForm)
        fragmentTransaction.show(fragmentSearch)
        fragmentTransaction.commit()
    }

    override fun setLoadingIndicator(active: Boolean) {}

    override fun getFormPresenter() = presenter.getFormPresenter()

    override fun getSearchPresenter() = presenter.getSearchPresenter()

    override fun onSearchResultItemClick(pasien: Pasien) {
        search_view.closeSearch()
        presenter.sendDataPasien(pasien)
    }

    private fun initFragments() {
        fragmentForm = FormPendaftaranFragment()
        fragmentSearch = SearchPasienFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.container_fragment, fragmentSearch, "search")
        fragmentTransaction.add(R.id.container_fragment, fragmentForm, "form")
        fragmentTransaction.commit()

        val formPresenter = FormPendaftaranPresenter(
                fragmentForm,
                PasienRepository.getInstance()
        )
        val searchPresenter = SearchPasienPresenter(
                fragmentSearch,
                PasienRepository.getInstance()
        )
        presenter = PendaftaranPresenter(
                this,
                formPresenter,
                searchPresenter
        )
    }

    private fun initViews() {
        search_view.setOnSearchViewListener(
                object : MaterialSearchView.SearchViewListener {
                    override fun onSearchViewClosed() {
                        showFormPage()
                    }

                    override fun onSearchViewShown() {
                        showSearchPage()
                    }
                }
        )
        search_view.setOnQueryTextListener(
                object : MaterialSearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        presenter.sendSearchQuery(query ?: "")
                        Log.d("SearchQuery", " - $query")
                        return true
                    }

                    override fun onQueryTextChange(newText: String?) = false
                }
        )
    }
}
