package um.teknik.informatika.simdok.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Dokter
import um.teknik.informatika.simdok.data.model.Petugas

class InfoProfilRepository private constructor() {

    private val mDatabase = FirebaseDatabase.getInstance()

    companion object {
        private var sInstance: InfoProfilRepository? = null
        @JvmStatic
        fun getInstance(): InfoProfilRepository {
            if (sInstance == null) sInstance = InfoProfilRepository()
            return sInstance!!
        }
    }

    /*fun pushProfilKlinik(klinik: Klinik, callback: BaseRepoCallback<String>) {
        val klinikRef = mDatabase.getReference("/klinik")
        klinikRef.setValue(klinik) { dbError, _ ->
            if (dbError == null) callback.onSuccess("Berhasil memperbarui data Klinik.")
            else callback.onFailed(dbError.message)
        }
    }

    fun getProfilKlinik(callback: BaseRepoCallback<Klinik>) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val klinik = snapshot.getValue(Klinik::class.java)
                    if (klinik == null) callback.onFailed("Terjadi kesalahan ketika menerima data Klinik.")
                    else callback.onSuccess(klinik)
                } else callback.onFailed("Tidak terdapat data Klinik di database.")
            }
        }
        val klinikRef = mDatabase.getReference("/klinik")
        klinikRef.addListenerForSingleValueEvent(listener)
    }*/

    fun getListDokter(callback: BaseRepoCallback<List<Dokter>>) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val list = arrayListOf<Dokter>()
                    for (child in snapshot.children) {
                        val dokter = child.getValue(Dokter::class.java)
                        if (dokter == null) {
                            callback.onFailed("Terjadi kesalahan ketika menerima data Dokter.")
                            return
                        } else {
                            dokter.uid = child.key ?: ""
                            list.add(dokter)
                        }
                    }
                    callback.onSuccess(list)
                } else callback.onFailed("Tidak terdapat data Dokter di database.")
            }
        }
        val dokterRef = mDatabase.getReference("/dokter")
        dokterRef.addListenerForSingleValueEvent(listener)
    }

    fun getListPetugas(callback: BaseRepoCallback<List<Petugas>>) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val list = arrayListOf<Petugas>()
                    for (child in snapshot.children) {
                        val petugas = child.getValue(Petugas::class.java)
                        if (petugas == null) {
                            callback.onFailed("Terjadi kesalahan ketika menerima data Petugas.")
                            return
                        } else {
                            petugas.uid = child.key ?: ""
                            list.add(petugas)
                        }
                    }
                    callback.onSuccess(list)
                } else callback.onFailed("Tidak terdapat data Petugas di database.")
            }
        }
        val petugasRef = mDatabase.getReference("/petugas")
        petugasRef.addListenerForSingleValueEvent(listener)
    }
}