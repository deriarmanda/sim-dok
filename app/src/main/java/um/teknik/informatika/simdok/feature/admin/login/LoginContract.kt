package um.teknik.informatika.simdok.feature.admin.login

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView

interface LoginContract {

    interface Presenter : BasePresenter {
        fun doLogin(email: String, password: String)
    }

    interface View : BaseView<Presenter> {
        fun onLoginSucceed()
        fun onLoginFailed(@StringRes msgRes: Int)
    }
}