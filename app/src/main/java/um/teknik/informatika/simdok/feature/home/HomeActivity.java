package um.teknik.informatika.simdok.feature.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.Antrean;
import um.teknik.informatika.simdok.data.repository.AccountRepository;
import um.teknik.informatika.simdok.data.repository.AntrianRepository;
import um.teknik.informatika.simdok.feature.admin.login.LoginActivity;
import um.teknik.informatika.simdok.feature.antrian.AntrianActivity;
import um.teknik.informatika.simdok.feature.bantuan.BantuanActivity;
import um.teknik.informatika.simdok.feature.profil.dokter.ProfilDokterActivity;
import um.teknik.informatika.simdok.feature.profil.klinik.ProfilKlinikActivity;
import um.teknik.informatika.simdok.feature.profil.petugas.ProfilPetugasActivity;
import um.teknik.informatika.simdok.feature.tentang.TentangActivity;
import um.teknik.informatika.simdok.util.DateTimeUtil;
import um.teknik.informatika.simdok.util.SharedPrefManager;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeContract.View {

    private final int RC_LOGIN = 110;

    private ProgressBar progressBar;
    private ViewFlipper flipperAntrean;

    private HomeContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        SharedPrefManager.init(getApplicationContext());

        presenter = new HomePresenter(
                this,
                AccountRepository.getInstance(),
                AntrianRepository.getInstance()
        );

        bindView();
        presenter.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.loadInfoAntrean();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_LOGIN && resultCode == RESULT_OK) openAdminHomePage();
        else super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_clinic)
            startActivity(new Intent(this, ProfilKlinikActivity.class));
        else if (id == R.id.nav_doctor)
            startActivity(new Intent(this, ProfilDokterActivity.class));
        else if (id == R.id.nav_petugas)
            startActivity(new Intent(this, ProfilPetugasActivity.class));
        else if (id == R.id.nav_queue_add)
            startActivity(new Intent(this, AntrianActivity.class));
        else if (id == R.id.nav_help)
            startActivity(new Intent(this, BantuanActivity.class));
        else if (id == R.id.nav_about)
            startActivity(new Intent(this, TentangActivity.class));
        else if (id == R.id.nav_login)
            startActivityForResult(
                    new Intent(this, LoginActivity.class),
                    RC_LOGIN
            );

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void openAdminHomePage() {
        Toast.makeText(this, R.string.login_msg_success, Toast.LENGTH_LONG).show();
        startActivity(new Intent(
                this,
                um.teknik.informatika.simdok.feature.admin.home.HomeActivity.class)
        );
        finish();
    }

    @Override
    public void onLoadInfoAntreanSucceed(@NotNull Antrean antrean, int currentAntrean) {
        flipperAntrean.setDisplayedChild(1);

        TextView text = flipperAntrean.findViewById(R.id.text_no_urut);
        text.setText(String.valueOf(antrean.getNoAntrean()));
        text = flipperAntrean.findViewById(R.id.text_identitas);
        text.setText(String.format(
                "%s / %s",
                antrean.getNama(),
                antrean.getTglLahir()
        ));
        text = flipperAntrean.findViewById(R.id.text_dokter_tujuan);
        text.setText(String.format(
                "Mengantri ke %s",
                antrean.getNamaDokter()
        ));
        text = flipperAntrean.findViewById(R.id.text_msg_antrean);
        String msg;
        if (currentAntrean == antrean.getNoAntrean()) {
            msg = "Nomor antrean anda telah dipanggil. Silahkan berangkat sekarang.";
        } else if (currentAntrean > antrean.getNoAntrean()) {
            msg = "Nomor antrean anda telah terlewati, silahkan mengantri ulang.";
        } else {
            msg = "Silahkan berangkat sekitar %d menit lagi."
                    + ((currentAntrean - antrean.getNoAntrean()) * 15);
        }
        text.setText(msg);
    }

    @Override
    public void onLoadInfoAntreanFailed(int msgRes) {
        Snackbar.make(findViewById(R.id.root), msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.loadInfoAntrean();
                    }
                }).show();
    }

    @Override
    public void showEmptyInfoAntrean() {
        flipperAntrean.setDisplayedChild(0);
    }

    @Override
    public HomeContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        progressBar.setVisibility((active ? View.VISIBLE : View.GONE));
    }

    private void bindView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        TextView navHeaderTanggal = header.findViewById(R.id.text_date);
        navHeaderTanggal.setText(DateTimeUtil.getHumanReadableDate(new Date()));

        progressBar = findViewById(R.id.progress_bar);
        flipperAntrean = findViewById(R.id.flipper_antrean);

        Button button = findViewById(R.id.button_more);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ProfilKlinikActivity.class));
            }
        });
        button = findViewById(R.id.button_dokter);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ProfilDokterActivity.class));
            }
        });
        button = findViewById(R.id.button_petugas);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ProfilPetugasActivity.class));
            }
        });
        button = flipperAntrean.findViewById(R.id.button_antre_sekarang);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, AntrianActivity.class));
            }
        });
    }
}
