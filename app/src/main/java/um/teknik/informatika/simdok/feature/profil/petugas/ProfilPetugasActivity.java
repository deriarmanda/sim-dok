package um.teknik.informatika.simdok.feature.profil.petugas;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.Petugas;

public class ProfilPetugasActivity extends AppCompatActivity implements ProfilPetugasContract.View {

    private ProfilPetugasContract.Presenter mPresenter;
    private RecyclerView rvPetugas;
    private SwipeRefreshLayout root;
    private LinearLayout layoutKontenKosong;
    private ProfilPetugasRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_petugas);

        mPresenter = new ProfilPetugasPresenter(this);
        bindView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    public void onLoadListPetugasSucceed(List<Petugas> petugasList) {
        layoutKontenKosong.setVisibility(View.GONE);
        rvPetugas.setVisibility(View.VISIBLE);
        adapter.setData(petugasList);
    }

    @Override
    public void showEmptyListMessage() {
        layoutKontenKosong.setVisibility(View.VISIBLE);
        rvPetugas.setVisibility(View.GONE);
    }

    @Override
    public void onLoadListPetugasFailed(int msgRes) {
        Toast.makeText(this, msgRes, Toast.LENGTH_LONG).show();
    }

    @Override
    public ProfilPetugasContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        root.setRefreshing(active);
    }

    private void bindView() {
        rvPetugas = findViewById(R.id.rv_profil_petugas);
        root = findViewById(R.id.swipe_refresh);
        layoutKontenKosong = findViewById(R.id.msg_empty);
        adapter = new ProfilPetugasRvAdapter();
        rvPetugas.setAdapter(adapter);
        rvPetugas.setLayoutManager(new LinearLayoutManager(this));
        root.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadListPetugasFromServer();
            }
        });
    }
}
