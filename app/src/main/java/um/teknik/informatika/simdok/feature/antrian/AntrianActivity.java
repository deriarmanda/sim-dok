package um.teknik.informatika.simdok.feature.antrian;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.Antrean;
import um.teknik.informatika.simdok.data.model.Dokter;
import um.teknik.informatika.simdok.data.repository.AntrianRepository;
import um.teknik.informatika.simdok.data.repository.InfoProfilRepository;
import um.teknik.informatika.simdok.util.DateTimeUtil;

public class AntrianActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, AntreanContract.View {

    private ViewFlipper container;
    private ProgressBar progressBar;
    private DatePickerDialog datePickerDialog;

    private Button buttonAntre;
    private TextInputEditText editAntreanNama;
    private TextInputEditText editAntreanAlamat;
    private TextInputEditText editAntreanTanggalLahir;
    private Spinner spinnerAntrean;
    private ArrayAdapter<String> spinnerAdapter;

    private AntreanContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_antrian);

        bindView();
        presenter = new AntreanPresenter(
                this,
                AntrianRepository.getInstance(),
                InfoProfilRepository.getInstance());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = DateFormat.getDateInstance(DateFormat.DEFAULT).format(calendar.getTime());

        editAntreanTanggalLahir.setText(currentDateString);
    }

    @Override
    public void showFormAntrianPage(@NonNull List<Dokter> dokterList) {
        spinnerAdapter.clear();
        for (Dokter dokter : dokterList) spinnerAdapter.add(dokter.getNama());
        spinnerAdapter.notifyDataSetChanged();

        container.setDisplayedChild(0);
    }

    @Override
    public void onLoadListDokterFailed(int msgRes) {
        Snackbar.make(container, msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.loadListDokter();
                    }
                }).show();
    }

    @Override
    public void showInfoAntrianPage() {
        container.setDisplayedChild(1);
    }

    @Override
    public void onInsertAntrianSucceed() {
        buttonAntre.setEnabled(true);
        Toast.makeText(this, R.string.antrean_msg_insert_success, Toast.LENGTH_LONG).show();
        presenter.loadAntrean();
    }

    @Override
    public void onInsertAntrianFailed(int msgRes) {
        buttonAntre.setEnabled(true);
        Snackbar.make(container, msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        buttonAntre.performClick();
                    }
                }).show();
    }

    @Override
    public void showInfoAntrean(@NonNull Antrean antrean, int currentAntrian) {
        showInfoAntrianPage();
        fetchAntrean(antrean, currentAntrian);
    }

    @Override
    public void onLoadAntreanFailed(int msgRes) {
        Snackbar.make(container, msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.loadAntrean();
                    }
                }).show();
    }

    @Override
    public AntreanContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        progressBar.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    private void bindView() {
        container = findViewById(R.id.container);
        progressBar = findViewById(R.id.progress_bar);
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(
                this,
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        // Form Antrian Views
        ImageView inputLayoutAlamat = findViewById(R.id.button_date);
        inputLayoutAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        editAntreanNama = findViewById(R.id.edit_antrean_nama);
        editAntreanAlamat = findViewById(R.id.edit_antrean_alamat);
        editAntreanTanggalLahir = findViewById(R.id.edit_antrean_tanggal_lahir);

        buttonAntre = findViewById(R.id.button_antre);
        buttonAntre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Antrean antrean = new Antrean(
                        DateTimeUtil.getSortableCurrentTime(new Date()),
                        editAntreanNama.getText().toString(),
                        editAntreanAlamat.getText().toString(),
                        editAntreanTanggalLahir.getText().toString(),
                        spinnerAntrean.getSelectedItem().toString(),
                        0, "", -1
                );
                buttonAntre.setEnabled(false);
                presenter.insertAntrean(antrean);
            }
        });

        spinnerAntrean = findViewById(R.id.spinner_antrean);
        spinnerAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item
        );
        spinnerAntrean.setAdapter(spinnerAdapter);
    }

    private void fetchAntrean(Antrean antrean, int currAntrean) {
        TextView text = findViewById(R.id.text_item_antrean_nama_pasien);
        text.setText(antrean.getNama());

        text = findViewById(R.id.text_item_antrean_tanggal_lahir_pasien);
        text.setText(antrean.getTglLahir());

        text = findViewById(R.id.text_item_antrean_alamat_pasien);
        text.setText(antrean.getAlamat());

        text = findViewById(R.id.text_item_pasien_dokter);
        text.setText(antrean.getNamaDokter());

        text = findViewById(R.id.text_item_antrean);
        text.setText(String.valueOf(antrean.getNoAntrean()));

        text = findViewById(R.id.text_item_antrean_id);
        text.setText(antrean.getIdAntrean());

        text = findViewById(R.id.text_item_antrean_sekarang);
        text.setText(String.valueOf(currAntrean));

        text = findViewById(R.id.text_item_antrean_estimasi);
        if (currAntrean > antrean.getNoAntrean()) {
            text.setText("0");
            presenter.removeCurrAntrean();
        } else text.setText(String.valueOf((currAntrean - antrean.getNoAntrean()) * 15));
    }

}