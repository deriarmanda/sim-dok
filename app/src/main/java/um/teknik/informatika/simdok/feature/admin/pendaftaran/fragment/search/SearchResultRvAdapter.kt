package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_search_result.view.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NAMA
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NO_RM

class SearchResultRvAdapter(
        private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<SearchResultRvAdapter.ItemViewHolder>() {

    var listResult = emptyList<Pasien>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var searchMode = SEARCH_BY_NO_RM
        set(value) {
            field = if (value != SEARCH_BY_NO_RM && value != SEARCH_BY_NAMA) SEARCH_BY_NO_RM
            else value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_search_result, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(listResult[position])
    }

    override fun getItemCount(): Int = listResult.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun fetch(pasien: Pasien) {
            itemView.image_pasien.setImageResource(
                    if (pasien.jenisKelamin == "Laki-laki") R.drawable.ic_medical_person_colored_80dp
                    else R.drawable.ic_medical_person_colored_80dp
            )
            itemView.text_title.text =
                    if (searchMode == SEARCH_BY_NO_RM) pasien.noRm
                    else pasien.nama
            itemView.text_subtitle.text =
                    if (searchMode == SEARCH_BY_NO_RM) pasien.nama
                    else pasien.noRm

            itemView.setOnClickListener { itemClickListener.onItemClick(pasien) }
        }
    }

    interface ItemClickListener {
        fun onItemClick(pasien: Pasien)
    }
}