package um.teknik.informatika.simdok.base

interface BaseView<T> {

    val presenter: T
    fun setLoadingIndicator(active: Boolean)
}