package um.teknik.informatika.simdok.feature.admin.pasien

import android.os.AsyncTask
import android.util.Log
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.repository.PasienRepository
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NAMA
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.search.SearchPasienContract.View.Companion.SEARCH_BY_NO_RM

class PasienPresenter(
        private val view: PasienContract.View,
        private val pasienRepo: PasienRepository
) : PasienContract.Presenter {

    override var list: List<Pasien> = emptyList()
    private var searchingTask = SearchingTask("", SEARCH_BY_NO_RM, view)

    override fun loadListPasien() {
        view.setLoadingIndicator(true)
        pasienRepo.getListPasien(
                object : BaseRepoCallback<List<Pasien>> {
                    override fun onSuccess(data: List<Pasien>) {
                        view.setLoadingIndicator(false)
                        list = data
                        if (list.isEmpty()) view.showEmptyListMessage()
                        else view.showListPasien(list)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadListPasienFailed(R.string.error_no_internet)
                        Log.d("PasienPresenter", msg)
                    }
                }
        )
    }

    override fun searchPasien(query: String, filter: Int) {
        if (query.isBlank()) {
            if (list.isEmpty()) view.showEmptyListMessage()
            else view.showListPasien(list)
        } else {
            if (!searchingTask.isCancelled) searchingTask.cancel(true)
            searchingTask = SearchingTask(query, filter, view)
            searchingTask.execute(list)
        }
    }

    override fun openDetailPage(pasien: Pasien) {
        view.showDetailPage(pasien)
    }

    override fun start() = loadListPasien()

    private class SearchingTask(
            private val query: String,
            private val filter: Int,
            private val view: PasienContract.View
    ) : AsyncTask<List<Pasien>, Void, List<Pasien>>() {

        override fun onPreExecute() {
            super.onPreExecute()
            view.setLoadingIndicator(true)
        }

        override fun doInBackground(vararg p0: List<Pasien>?): List<Pasien> {
            val list = p0[0]!!
            val result = arrayListOf<Pasien>()
            for (pasien in list) {
                if (filter == SEARCH_BY_NO_RM && pasien.noRm.contains(query, true)) {
                    result.add(pasien)
                } else if (filter == SEARCH_BY_NAMA && pasien.nama.contains(query, true)) {
                    result.add(pasien)
                }
            }
            return result
        }

        override fun onPostExecute(result: List<Pasien>?) {
            super.onPostExecute(result)
            view.setLoadingIndicator(false)
            if (result != null) {
                if (result.isEmpty()) view.showEmptyListMessage()
                else view.showListPasien(result)
            } else {
                view.onLoadListPasienFailed(R.string.pasien_error_unknown_result_search)
            }
        }

    }
}