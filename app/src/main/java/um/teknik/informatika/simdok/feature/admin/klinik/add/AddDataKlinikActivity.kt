package um.teknik.informatika.simdok.feature.admin.klinik.add

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_data_klinik.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.DataKlinik
import um.teknik.informatika.simdok.data.repository.DataKlinikRepository

class AddDataKlinikActivity : AppCompatActivity(), AddDataKlinikContract.View {

    override lateinit var presenter: AddDataKlinikContract.Presenter
    private lateinit var readableType: String

    companion object {
        private const val EXTRA_TYPE = "extra_type_data_klinik"
        @JvmStatic
        fun getIntent(context: Context, type: String): Intent {
            val intent = Intent(context, AddDataKlinikActivity::class.java)
            intent.putExtra(EXTRA_TYPE, type)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_data_klinik)

        val type = intent.getStringExtra(EXTRA_TYPE)
        readableType = type.substring(1).capitalize()
        supportActionBar?.title = getString(
                R.string.title_tambah_data_klinik,
                readableType
        )

        input_layout_kode.hint = getString(R.string.dataklinik_hint_kode, readableType)
        input_layout_nama.hint = getString(R.string.dataklinik_hint_nama, readableType)
        button_simpan.setOnClickListener {
            presenter.saveDataKlinik(DataKlinik(
                    form_kode.text.toString(),
                    form_nama.text.toString()
            ))
        }

        presenter = AddDataKlinikPresenter(
                this,
                type,
                DataKlinikRepository.getInstance()
        )
    }

    override fun onDataKlinikSaved() {
        Toast.makeText(this, "Berhasil menambahkan data $readableType.", Toast.LENGTH_SHORT).show()
        form_kode.setText("")
        form_nama.setText("")
    }

    override fun onSaveDataKlinikFailed(message: String) {
        Toast.makeText(
                this,
                "Gagal menambahkan data $readableType. Periksa internet anda.",
                Toast.LENGTH_LONG
        ).show()
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

}
