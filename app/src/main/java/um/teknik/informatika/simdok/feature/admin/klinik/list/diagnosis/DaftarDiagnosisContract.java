package um.teknik.informatika.simdok.feature.admin.klinik.list.diagnosis;

import android.support.annotation.StringRes;

import java.util.List;

import um.teknik.informatika.simdok.base.BasePresenter;
import um.teknik.informatika.simdok.base.BaseView;
import um.teknik.informatika.simdok.data.model.DataKlinik;

public interface DaftarDiagnosisContract {

    interface Presenter extends BasePresenter {
        void loadListDiagnosisFromServer();

        void deleteSelectedDiagnosis(DataKlinik diagnosis);
    }

    interface View extends BaseView<Presenter> {
        void onLoadListDiagnosisSucceed(List<DataKlinik> dataKlinikList);
        void onLoadListDiagnosisFailed(@StringRes int msgRes);

        void showEmptyListMessage();

        void onDeleteDiagnosisSucceed();

        void onDeleteDiagnosisFailed();
    }
}
