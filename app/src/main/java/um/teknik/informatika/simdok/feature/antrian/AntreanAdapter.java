package um.teknik.informatika.simdok.feature.antrian;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import um.teknik.informatika.simdok.data.model.Dokter;

public class AntreanAdapter extends ArrayAdapter<Dokter> {

    public AntreanAdapter(Context context, ArrayList<Dokter> dokter) {
        super(context, 0, dokter);

    }
}
