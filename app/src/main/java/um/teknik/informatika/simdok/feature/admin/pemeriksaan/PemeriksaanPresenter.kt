package um.teknik.informatika.simdok.feature.admin.pemeriksaan

import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.model.Pemeriksaan
import um.teknik.informatika.simdok.data.repository.PemeriksaanRepository

class PemeriksaanPresenter(
        private val view: PemeriksaanContract.View,
        private val pasien: Pasien,
        private val pemeriksaanRepo: PemeriksaanRepository
) : PemeriksaanContract.Presenter {

    override fun loadListPemeriksaan() {
        view.setLoadingIndicator(true)
        pemeriksaanRepo.getListPemeriksaan(
                pasien.uid,
                object : BaseRepoCallback<List<Pemeriksaan>> {
                    override fun onSuccess(data: List<Pemeriksaan>) {
                        view.setLoadingIndicator(false)
                        if (data.isEmpty()) view.showEmptyListMessage()
                        else view.showListPemeriksaan(data)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadListPemeriksaanFailed(msg)
                    }
                }
        )
    }

    override fun deletePemeriksaan(pemeriksaan: Pemeriksaan) {
        view.setLoadingIndicator(true)
        pemeriksaanRepo.deletePemeriksaan(
                pasien.uid,
                pemeriksaan,
                object : BaseRepoCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        view.setLoadingIndicator(false)
                        view.onDeletePasienSuccess()
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onDeletePasienSuccess()
                    }
                }
        )
    }

    override fun start() = loadListPemeriksaan()
}