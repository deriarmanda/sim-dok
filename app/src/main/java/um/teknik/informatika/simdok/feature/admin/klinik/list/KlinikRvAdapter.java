package um.teknik.informatika.simdok.feature.admin.klinik.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import um.teknik.informatika.sim_dok.R;
import um.teknik.informatika.simdok.data.model.DataKlinik;

public class KlinikRvAdapter extends RecyclerView.Adapter<KlinikRvAdapter.MyViewHolder> {

    private List<DataKlinik> mData;
    private OnButtonDeleteClickListener btnDeleteClickListener;

    public KlinikRvAdapter(OnButtonDeleteClickListener btnDeleteClickListener) {
        this.btnDeleteClickListener = btnDeleteClickListener;
        mData = new ArrayList<>();
    }

    public void setData(List<DataKlinik> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_klinik, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DataKlinik dataKlinik = mData.get(position);
        holder.textKode.setText(dataKlinik.getKode());
        holder.textNama.setText(dataKlinik.getNama());
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnDeleteClickListener != null) btnDeleteClickListener.onClick(dataKlinik);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public interface OnButtonDeleteClickListener {
        void onClick(DataKlinik dataKlinik);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textKode;
        TextView textNama;
        ImageView btnDelete;

        MyViewHolder(View itemView) {
            super(itemView);

            textKode = itemView.findViewById(R.id.text_kode);
            textNama = itemView.findViewById(R.id.text_nama);
            btnDelete = itemView.findViewById(R.id.button_delete);
        }
    }
}
