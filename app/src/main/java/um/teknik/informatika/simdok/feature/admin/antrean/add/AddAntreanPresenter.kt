package um.teknik.informatika.simdok.feature.admin.antrean.add

import android.util.Log
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Antrean
import um.teknik.informatika.simdok.data.model.Dokter
import um.teknik.informatika.simdok.data.repository.InfoProfilRepository
import um.teknik.informatika.simdok.data.repository.ManageAntreanRepository

class AddAntreanPresenter(
        private val view: AddAntreanContract.View,
        private val antreanRepo: ManageAntreanRepository,
        private val infoProfilRepo: InfoProfilRepository
) : AddAntreanContract.Presenter {

    override fun loadListDokter() {
        view.setLoadingIndicator(true)
        infoProfilRepo.getListDokter(
                object : BaseRepoCallback<List<Dokter>> {
                    override fun onSuccess(data: List<Dokter>) {
                        view.setLoadingIndicator(false)
                        view.showListDokter(data)
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadListDokterFailed()
                        Log.d("AddAntreanPresenter", msg)
                    }
                }
        )
    }

    override fun insertAntrean(antrean: Antrean) {
        view.setLoadingIndicator(true)
        antreanRepo.insertAntrean(
                antrean,
                object : BaseRepoCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        view.setLoadingIndicator(false)
                        view.onInsertAntreanSuccess()
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onInsertAntreanFailed()
                    }
                }
        )
    }

    override fun start() = loadListDokter()
}