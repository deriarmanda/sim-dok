package um.teknik.informatika.simdok.feature.splashscreen

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView

interface SplashScreenContract {

    interface Presenter : BasePresenter {
        fun checkCurrentUser()
        fun loginAnonymously()
    }

    interface View : BaseView<Presenter> {
        fun onLoginSucceed()
        fun onLoginFailed(@StringRes msgRes: Int)
    }

}