package um.teknik.informatika.simdok.feature.splashscreen

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_splash_screen.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.feature.home.HomeActivity
import um.teknik.informatika.simdok.util.SharedPrefManager

class SplashScreenActivity : AppCompatActivity(), SplashScreenContract.View {

    override lateinit var presenter: SplashScreenContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        SharedPrefManager.init(applicationContext)

        presenter = SplashScreenPresenter(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onLoginSucceed() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    override fun onLoginFailed(msgRes: Int) {
        Snackbar.make(root, msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry) {
                    presenter.loginAnonymously()
                }
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if(active) View.VISIBLE else View.INVISIBLE
    }
}
