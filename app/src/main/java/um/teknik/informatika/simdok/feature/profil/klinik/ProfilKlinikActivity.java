package um.teknik.informatika.simdok.feature.profil.klinik;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import um.teknik.informatika.sim_dok.R;

public class

ProfilKlinikActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_klinik);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.app_nama_klinik);
            actionBar.setSubtitle(R.string.app_alamat_klinik);
        }
    }

}
