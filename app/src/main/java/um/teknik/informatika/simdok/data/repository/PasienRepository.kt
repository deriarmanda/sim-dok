package um.teknik.informatika.simdok.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pasien

class PasienRepository private constructor() {

    private val mDbReference = FirebaseDatabase.getInstance().getReference("/pasien")

    companion object {
        private var sInstance: PasienRepository? = null
        @JvmStatic
        fun getInstance(): PasienRepository {
            if (sInstance == null) sInstance = PasienRepository()
            return sInstance!!
        }
    }

    fun insertPasienBaru(pasien: Pasien, callback: BaseRepoCallback<Pasien>) {
        mDbReference.push().setValue(pasien) { dbError, dbRef ->
            if (dbError != null) callback.onFailed(dbError.message)
            else {
                val uid = dbRef.key ?: ""
                pasien.uid = uid
                callback.onSuccess(pasien)
            }
        }
    }

    fun updatePasienLama(pasien: Pasien, callback: BaseRepoCallback<Pasien>) {
        mDbReference.child(pasien.uid).updateChildren(mapOf(
                Pair("/noRM", pasien.noRm),
                Pair("/nama", pasien.nama),
                Pair("/tempatLahir", pasien.tempatLahir),
                Pair("/tglLahir", pasien.tglLahir),
                Pair("/alamat", pasien.alamat),
                Pair("/telp", pasien.telp),
                Pair("/jenisKelamin", pasien.jenisKelamin),
                Pair("/golDarah", pasien.golDarah),
                Pair("/pekerjaan", pasien.pekerjaan),
                Pair("/status", pasien.status),
                Pair("/agama", pasien.agama),
                Pair("/tglBerkunjung", pasien.tglBerkunjung)
        )) { dbError, _ ->
            if (dbError != null) callback.onFailed(dbError.message)
            else callback.onSuccess(pasien)
        }
    }

    fun deletePasien(pasien: Pasien, callback: BaseRepoCallback<Unit>) {
        mDbReference.child(pasien.uid).removeValue { dbError, _ ->
            if (dbError != null) callback.onFailed(dbError.message)
            else callback.onSuccess(Unit)
        }
    }

    fun getListPasien(callback: BaseRepoCallback<List<Pasien>>) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onFailed(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val list = arrayListOf<Pasien>()
                    for (data in snapshot.children) {
                        val pasien = data.getValue(Pasien::class.java)
                        val key = data.key ?: ""
                        if (pasien != null) {
                            pasien.uid = key
                            list.add(pasien)
                        } else {
                            callback.onFailed("NPE occured while trying to " +
                                    "parse Data Pasien with uid: $key")
                            return
                        }
                    }
                    callback.onSuccess(list)
                } else callback.onSuccess(emptyList())
            }
        }

        val sortedRef = mDbReference.orderByChild("tglBerkunjung")
        sortedRef.addListenerForSingleValueEvent(listener)
    }
}