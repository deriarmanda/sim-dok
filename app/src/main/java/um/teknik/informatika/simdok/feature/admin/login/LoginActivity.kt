package um.teknik.informatika.simdok.feature.admin.login

import android.app.Activity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.repository.AccountRepository

class LoginActivity : AppCompatActivity(), LoginContract.View {

    override lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(this, AccountRepository.getInstance())

        button_login.setOnClickListener {
            presenter.doLogin(
                    form_email.text.toString(),
                    form_password.text.toString()
            )
        }
    }

    override fun onLoginSucceed() {
        //Toast.makeText(this, R.string.login_msg_success, Toast.LENGTH_LONG).show()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onLoginFailed(msgRes: Int) {
        val snackbar = Snackbar.make(container_form, msgRes, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.action_tutup) {
            snackbar.dismiss()
        }.show()
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }
}
