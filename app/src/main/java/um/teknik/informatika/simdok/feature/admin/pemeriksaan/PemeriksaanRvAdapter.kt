package um.teknik.informatika.simdok.feature.admin.pemeriksaan

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_list_pemeriksaan.view.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pemeriksaan
import um.teknik.informatika.simdok.util.DateTimeUtil

class PemeriksaanRvAdapter(
        private val buttonDeleteClickListener: OnButtonDeleteClickListener
) : RecyclerView.Adapter<PemeriksaanRvAdapter.ViewHolder>() {

    var list = listOf<Pemeriksaan>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_pemeriksaan, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(list[position])
    }

    override fun getItemCount() = list.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(pemeriksaan: Pemeriksaan) {
            itemView.text_date.text = DateTimeUtil.formatHumanReadableDateString(pemeriksaan.tanggal)
            itemView.text_anamnesa.text = pemeriksaan.anamnesa
            itemView.text_diagnosa.text = pemeriksaan.diagnosa
            itemView.text_tindakan.text = pemeriksaan.tindakan
            itemView.text_obat.text = pemeriksaan.obat
            itemView.button_delete.setOnClickListener {
                buttonDeleteClickListener.onClick(pemeriksaan)
            }
        }
    }

    interface OnButtonDeleteClickListener {
        fun onClick(pemeriksaan: Pemeriksaan)
    }
}