package um.teknik.informatika.simdok.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude

data class Pasien(
        val noRm: String = "",
        val nama: String = "",
        val tempatLahir: String = "",
        val tglLahir: String = "",
        val alamat: String = "",
        val telp: String = "",
        val jenisKelamin: String = "",
        val golDarah: String = "",
        val pekerjaan: String = "",
        val status: String = "",
        val agama: String = "",
        var tglBerkunjung: String = "",
        @get:Exclude var uid: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(noRm)
        parcel.writeString(nama)
        parcel.writeString(tempatLahir)
        parcel.writeString(tglLahir)
        parcel.writeString(alamat)
        parcel.writeString(telp)
        parcel.writeString(jenisKelamin)
        parcel.writeString(golDarah)
        parcel.writeString(pekerjaan)
        parcel.writeString(status)
        parcel.writeString(agama)
        parcel.writeString(tglBerkunjung)
        parcel.writeString(uid)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pasien> {
        override fun createFromParcel(parcel: Parcel): Pasien {
            return Pasien(parcel)
        }

        override fun newArray(size: Int): Array<Pasien?> {
            return arrayOfNulls(size)
        }
    }
}