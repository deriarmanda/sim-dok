package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran

import android.support.annotation.StringRes
import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Pasien

interface FormPendaftaranContract {

    interface Presenter : BasePresenter {
        fun loadDataPasien(pasien: Pasien)
        fun savePasienBaru(pasien: Pasien)
        fun updatePasienLama(pasien: Pasien)
    }

    interface View : BaseView<Presenter> {
        fun onSavePasienSuccess(pasien: Pasien)
        fun onSavePasienFailed(@StringRes msgRes: Int)
        fun fetchDataPasien(pasien: Pasien)
    }
}