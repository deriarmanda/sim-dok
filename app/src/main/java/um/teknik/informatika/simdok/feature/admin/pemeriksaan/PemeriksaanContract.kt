package um.teknik.informatika.simdok.feature.admin.pemeriksaan

import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.Pemeriksaan

interface PemeriksaanContract {

    interface Presenter : BasePresenter {
        fun loadListPemeriksaan()
        fun deletePemeriksaan(pemeriksaan: Pemeriksaan)
    }

    interface View : BaseView<Presenter> {
        fun showListPemeriksaan(list: List<Pemeriksaan>)
        fun showEmptyListMessage()
        fun onLoadListPemeriksaanFailed(message: String)
        fun onDeletePasienSuccess()
        fun onDeletePasienFailed(message: String)
        fun showAddPemeriksaanPage()
    }
}