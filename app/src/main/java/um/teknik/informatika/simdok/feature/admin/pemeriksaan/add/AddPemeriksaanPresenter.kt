package um.teknik.informatika.simdok.feature.admin.pemeriksaan.add

import um.teknik.informatika.simdok.base.BaseRepoCallback
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.data.model.Pemeriksaan
import um.teknik.informatika.simdok.data.repository.PemeriksaanRepository

class AddPemeriksaanPresenter(
        private val view: AddPemeriksaanContract.View,
        private val pasien: Pasien,
        private val pemeriksaanRepo: PemeriksaanRepository
) : AddPemeriksaanContract.Presenter {

    override fun savePemeriksaan(pemeriksaan: Pemeriksaan) {
        view.setLoadingIndicator(true)
        pemeriksaanRepo.insertPemeriksaan(
                pasien.uid,
                pemeriksaan,
                object : BaseRepoCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        view.setLoadingIndicator(false)
                        view.onPemeriksaanSaved()
                    }

                    override fun onFailed(msg: String) {
                        view.setLoadingIndicator(false)
                        view.onSavePemeriksaanFailed(msg)
                    }
                }
        )
    }

    override fun start() {}
}