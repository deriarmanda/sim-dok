package um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.pendaftaran

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_form_pendaftaran.*
import um.teknik.informatika.sim_dok.R
import um.teknik.informatika.simdok.data.model.Pasien
import um.teknik.informatika.simdok.feature.admin.pendaftaran.fragment.FragmentsInteractionListener
import um.teknik.informatika.simdok.util.DateTimeUtil
import java.util.*

class FormPendaftaranFragment : Fragment(), FormPendaftaranContract.View {

    override lateinit var presenter: FormPendaftaranContract.Presenter
    private lateinit var parentListener: FragmentsInteractionListener
    private var pasien: Pasien? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentsInteractionListener) {
            parentListener = context
        } else throw IllegalArgumentException("Parent must be implement FragmentsInteractionListener first !")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_pendaftaran, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_simpan.setOnClickListener {
            val pasienBaru = Pasien(
                    form_no_rm.text.toString(),
                    form_nama.text.toString(),
                    form_tempat_lahir.text.toString(),
                    form_tanggal_lahir.text.toString(),
                    form_alamat.text.toString(),
                    form_telepon.text.toString(),
                    if (radio_laki_laki.isChecked) "Laki-laki" else "Perempuan",
                    form_gol_darah.text.toString(),
                    form_pekerjaan.text.toString(),
                    form_status.text.toString(),
                    form_agama.text.toString(),
                    DateTimeUtil.getSortableCurrentDate(Date())
            )

            if (pasien == null || pasien?.uid.isNullOrEmpty()) {
                pasien = pasienBaru
                presenter.savePasienBaru(pasien!!)
            } else {
                pasienBaru.uid = pasien!!.uid
                presenter.updatePasienLama(pasienBaru)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = parentListener.getFormPresenter()
    }

    override fun onSavePasienSuccess(pasien: Pasien) {
        Toast.makeText(context, R.string.pasien_msg_save_success, Toast.LENGTH_SHORT).show()
        clearForm()
    }

    override fun onSavePasienFailed(msgRes: Int) {
        Snackbar.make(button_simpan, msgRes, Snackbar.LENGTH_LONG)
                .setAction(R.string.action_retry) {
                    button_simpan.performClick()
                }
    }

    override fun fetchDataPasien(pasien: Pasien) {
        this.pasien = pasien
        form_no_rm.setText(pasien.noRm)
        form_nama.setText(pasien.nama)
        form_tempat_lahir.setText(pasien.tempatLahir)
        form_tanggal_lahir.setText(pasien.tglLahir)
        form_alamat.setText(pasien.alamat)
        form_telepon.setText(pasien.telp)
        radio_laki_laki.isChecked = pasien.jenisKelamin == "Laki-laki"
        radio_perempuan.isChecked = !radio_laki_laki.isChecked
        form_gol_darah.setText(pasien.golDarah)
        form_pekerjaan.setText(pasien.pekerjaan)
        form_status.setText(pasien.status)
        form_agama.setText(pasien.agama)
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

    private fun clearForm() {
        form_no_rm.setText("")
        form_nama.setText("")
        form_tempat_lahir.setText("")
        form_tanggal_lahir.setText("")
        form_alamat.setText("")
        form_telepon.setText("")
        radio_laki_laki.isChecked = true
        radio_perempuan.isChecked = false
        form_gol_darah.setText("")
        form_pekerjaan.setText("")
        form_status.setText("")
        form_agama.setText("")
    }
}
