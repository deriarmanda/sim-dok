package um.teknik.informatika.simdok.util

import android.content.Context
import android.content.SharedPreferences

object SharedPrefManager {

    private const val PREF_NAME = "pref_sim_dok"
    private const val PREF_KEY_ANTREAN_UID = "uid_current_antrean"

    @JvmStatic
    private lateinit var sharedPref: SharedPreferences

    @JvmStatic
    fun init(context: Context) {
        sharedPref = context.getSharedPreferences(PREF_NAME, 0)
    }

    @JvmStatic
    fun getAntreanUid() = sharedPref.getString(PREF_KEY_ANTREAN_UID, "")

    @JvmStatic
    fun saveAntreanUid(uid: String) {
        val editor = sharedPref.edit()
        editor.putString(PREF_KEY_ANTREAN_UID, uid)
        editor.apply()
    }

    @JvmStatic
    fun removeAntreanUid() {
        val editor = sharedPref.edit()
        editor.remove(PREF_KEY_ANTREAN_UID)
        editor.apply()
    }
}