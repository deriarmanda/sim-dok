package um.teknik.informatika.simdok.feature.admin.klinik.add

import um.teknik.informatika.simdok.base.BasePresenter
import um.teknik.informatika.simdok.base.BaseView
import um.teknik.informatika.simdok.data.model.DataKlinik

interface AddDataKlinikContract {

    interface Presenter : BasePresenter {
        fun saveDataKlinik(data: DataKlinik)
    }

    interface View : BaseView<Presenter> {
        fun onDataKlinikSaved()
        fun onSaveDataKlinikFailed(message: String)
    }
}